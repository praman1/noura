//
//  OrderDetailsViewController.h
//  Noura App
//
//  Created by volive solutions on 21/10/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface OrderDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTotalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UITableView *orderDetailsTableView;


-(void)orderDetailsServiceCall:(NSString *)orderId;
@property NSMutableArray *ordersIdArray;
@property (weak, nonatomic) IBOutlet UILabel *shippingToLabel;
@property (weak, nonatomic) IBOutlet UILabel *yourOrdersLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingLabel;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentSummeryLabel;

- (IBAction)backBtn:(id)sender;

@end
