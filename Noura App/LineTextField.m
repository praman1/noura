//
//  LineTextField.m
//  JSON
//
//  Created by Volivesolutions on 11/25/16.
//  Copyright (c) 2016 Volivesolutions. All rights reserved.
//

#import "LineTextField.h"

@implementation LineTextField


-(void)textfieldAsLine:(UITextField *)myTextfield
             lineColor:(UIColor *)lineColor
           placeHolder:(NSString *)placeholder
                myView:(UIView *)view
{
    
    
    [myTextfield setBorderStyle:UITextBorderStyleNone];
    myTextfield.placeholder=placeholder;
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.0;
    border.borderColor = lineColor.CGColor;
    border.frame = CGRectMake(0, myTextfield.frame.size.height - borderWidth, view.frame.size.width, myTextfield.frame.size.height);
    border.borderWidth = borderWidth;
    [myTextfield.layer addSublayer:border];
    myTextfield.layer.masksToBounds = YES;

}



@end
