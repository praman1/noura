//
//  ViewAllController.m
//  Noura App
//
//  Created by volive solutions on 27/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ViewAllController.h"
#import "ViewAllCell.h"
#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HCSStarRatingView.h"
#import "ProductInfoViewController.h"
@interface ViewAllController ()
{
    
    NSMutableArray * productsImageArr;
    NSMutableArray *productsNameArr;
    NSMutableArray *productPriceArr;
    NSMutableArray *productOldArr;
    NSMutableArray * ratingArr;
    NSMutableArray * productIdArr;
    
    NSMutableArray *productsArrayCount;
    SharedClass *objForsharedClass;
}

@end

@implementation ViewAllController

- (void)viewDidLoad {
    [super viewDidLoad];
    objForsharedClass = [[SharedClass alloc] init];
    NSLog(@"productIdStr value is %@",_productIdStr);
    
    productsImageArr =[NSMutableArray new];
    
    productsNameArr =[NSMutableArray new];
    productPriceArr =[NSMutableArray new];
    productOldArr =[NSMutableArray new];
    ratingArr =[NSMutableArray new];
    productIdArr=[NSMutableArray new];
    
    productsArrayCount = [NSMutableArray new];
    
    [_viewAllCollectionView reloadData];
    
    [self viewAllServiceCall];
    
    
}

#pragma mark ViewAll Servicecall
-(void)viewAllServiceCall
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/noura_services/services/similar_products?product_id=13&store_id=1
    NSMutableDictionary *productsPostDictionary = [[NSMutableDictionary alloc]init];
    [productsPostDictionary setObject:_productIdStr  forKey:@"product_id"];
    
    [productsPostDictionary setObject:languageStr forKey:@"store_id"];
    
     //[productsPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"similar_products?" withPostDict:productsPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                productsArrayCount=[dataDictionary objectForKey:@"data"];
                
                if (productsArrayCount.count>0) {
                    for (int i=0; i<productsArrayCount.count; i++) {
                        [productsImageArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [productsNameArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [productPriceArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                        //[productOldArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                        [ratingArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"rating"]];
                        
                         [productIdArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        
                        // NSLog(@"%@",_clothsList);
                    }
                }
                [_viewAllCollectionView reloadData];
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}

#pragma mark CollectionView Delegate Methods
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  
        return productsNameArr.count;
   
    }

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
//    if (collectionView == _viewAllCollectionView) {
    
        ViewAllCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"viewAllCell" forIndexPath:indexPath];
        cell.cellView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        cell.cellView.layer.borderWidth = 1.0f;

        [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[productsImageArr objectAtIndex:indexPath.row]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
        cell.productNameLabel.text = [productsNameArr objectAtIndex:indexPath.row];
        cell.presentAmountLabel.text = [NSString stringWithFormat:@"%@",[productPriceArr objectAtIndex:indexPath.row]];
//        cell.oldAmountLabel.text = [productOldArr objectAtIndex:indexPath.row];
    
        cell.productRatingView.value = [[ratingArr objectAtIndex:indexPath.row]intValue];
    
        cell.productRatingView.userInteractionEnabled=NO;
    
        return cell;
        
    }
    
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //if (collectionView == _viewAllCollectionView) {
        CGFloat width = (CGFloat) (_viewAllCollectionView.frame.size.width/2);
        
        return CGSizeMake(width-5,240);
    }

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == _viewAllCollectionView){
       
        ProductInfoViewController *productsList = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductInfoViewController"];
        [productsList productInfoServiceCall:[productIdArr objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:productsList animated:YES];
            
        }
   }

- (IBAction)barBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
