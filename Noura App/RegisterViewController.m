//
//  RegisterViewController.m
//  Noura App
//
//  Created by volive solutions on 19/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "SWRevealViewController.h"
#import "SharedClass.h"


@interface RegisterViewController ()
{
    NSString *genderString;
    BOOL isChecked;
    
    //NSString * status;
    NSString * message;
   // SharedClass *obj;

    
   // SharedClass *objForSharedclass;
}

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
        [self loadViewWithCustomDesign];
    
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {

   // obj = [[SharedClass alloc] init];
    
    isChecked = NO;
    _nameTF.delegate = self;
    _emailTF.delegate = self;
    _passwordTF.delegate = self;
    _confirmPasswordTF.delegate = self;
    _mobileNumberTF.delegate = self;
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _nameTF.textAlignment = NSTextAlignmentRight;
        _emailTF.textAlignment = NSTextAlignmentRight;
        _passwordTF.textAlignment = NSTextAlignmentRight;
        _confirmPasswordTF.textAlignment = NSTextAlignmentRight;
        _mobileNumberTF.textAlignment = NSTextAlignmentRight;
        
    } else  {
        
        _nameTF.textAlignment = NSTextAlignmentLeft;
        _emailTF.textAlignment = NSTextAlignmentLeft;
        _passwordTF.textAlignment = NSTextAlignmentLeft;
        _confirmPasswordTF.textAlignment = NSTextAlignmentLeft;
        _mobileNumberTF.textAlignment = NSTextAlignmentLeft;
    }
    
    
    _nameTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _nameTF.layer.borderWidth = 1.2f;
    _emailTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _emailTF.layer.borderWidth = 1.2f;
    _passwordTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _passwordTF.layer.borderWidth = 1.2f;
    _confirmPasswordTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _confirmPasswordTF.layer.borderWidth = 1.2f;
    _mobileNumberTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _mobileNumberTF.layer.borderWidth = 1.2f;
    UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [self.view addGestureRecognizer:tap];
    
    _signUpBtn.layer.masksToBounds = false;
    _signUpBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _signUpBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _signUpBtn.layer.shadowRadius = 3;
    _signUpBtn.layer.shadowOpacity = 0.5;
    
    _nameTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Name"];
    _emailTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Email Address"];
    _passwordTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"PassWord"];
    _confirmPasswordTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Confirm PassWord"];
    _mobileNumberTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Mobile Number"];
    
    _maleLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Male"];
    _femaleLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"FeMale"];
    
    [_signUpBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"SignUp"] forState:UIControlStateNormal];
    [_alreadyAMemberBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Already A Member? Sign In"] forState:UIControlStateNormal];


}


#pragma mark - textfield delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _nameTF) {
        
        _nameTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _emailTF) {
        _emailTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _passwordTF) {
        _passwordTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _confirmPasswordTF) {
        _confirmPasswordTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _mobileNumberTF) {
        _mobileNumberTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if (textField == _nameTF) {
        _nameTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _emailTF){
        _emailTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _passwordTF){
       _passwordTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _confirmPasswordTF){
        _confirmPasswordTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _mobileNumberTF){
        _mobileNumberTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    [_nameTF resignFirstResponder];
    [_emailTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    [_confirmPasswordTF resignFirstResponder];
    [_mobileNumberTF resignFirstResponder];
    
    [_signUpScrollview setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _nameTF) {
        
        [_emailTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _emailTF) {
        
        [_passwordTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _passwordTF) {
        
        [_confirmPasswordTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _confirmPasswordTF) {
        
        [_mobileNumberTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _mobileNumberTF) {
        
        [_mobileNumberTF resignFirstResponder];
        
        returnValue = YES;
        
    }
    
    return returnValue;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Signup Servicecall
-(void)signUpServiceCall
{
            [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
            [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
            [SVProgressHUD showWithStatus:@"Loading..."];
    
    //NSString *url = @"https://lbcii.com/services/services/registration";
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    NSMutableDictionary * registerPostDictionary = [[NSMutableDictionary alloc]init];
    
    //[registerPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"] forKey:@"customer_id"];
    // [registerPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"emailId"] forKey:@"email"];
    [registerPostDictionary setObject:_emailTF.text forKey:@"email"];
    [registerPostDictionary setObject:_passwordTF.text forKey:@"password"];
    [registerPostDictionary setObject:_nameTF.text forKey:@"name"];
    [registerPostDictionary setObject:_mobileNumberTF.text forKey:@"mobile"];
    [registerPostDictionary setObject:genderString forKey:@"gender"];
    [registerPostDictionary setObject:languageStr forKey:@"store_id"];
    
    
    [[SharedClass sharedInstance]urlPerameterforPost:@"registration" withPostDict:registerPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Images from server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([status isEqualToString:@"1"])
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Success"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:^{  [self.navigationController popViewControllerAnimated:TRUE];}];
                    });

                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[SharedClass sharedInstance] showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                    
                }
            });
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[SharedClass sharedInstance] showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Error"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            });
        }
    }];
    
}

- (IBAction)signUpBtn:(id)sender {
    [_nameTF resignFirstResponder];
    [_emailTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    [_confirmPasswordTF resignFirstResponder];
    [_mobileNumberTF resignFirstResponder];
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if (self.nameTF.text.length < 3 ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter a valid name"] onViewController:self completion:^{  [self.nameTF becomeFirstResponder]; }];
        
    } else if ([emailTest evaluateWithObject:self.emailTF.text] == NO ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter valid email id"] onViewController:self completion:^{  [self.emailTF becomeFirstResponder]; }];
        
    } else if (self.passwordTF.text.length < 5 ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Password should be more than 5 characters"] onViewController:self completion:^{  [self.passwordTF becomeFirstResponder]; }];
        
    } else if (![self.confirmPasswordTF.text isEqualToString:self.passwordTF.text] ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Password and Confirm Password must be same"] onViewController:self completion:^{  [self.confirmPasswordTF becomeFirstResponder]; }];
        
    } else if (self.mobileNumberTF.text.length < 5 ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Mobile number should be more than 5 digits"] onViewController:self completion:^{  [self.mobileNumberTF becomeFirstResponder]; }];
        
    } else {
        
        [self signUpServiceCall];
        
    }

    
}
- (IBAction)alreadyAMemberBtn:(id)sender {
//    LoginViewController *logIn =[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Male and Female Buttons
- (IBAction)maleButton:(id)sender {
     isChecked=!isChecked;
    if(isChecked == YES){
    _maleCheckBox.image = [UIImage imageNamed:@"check2"];
    _femaleCheckBox.image = [UIImage imageNamed:@"checkbox"];
    }
    genderString = [NSString stringWithFormat:@"1"];
}

- (IBAction)femaleButton:(id)sender {
    isChecked=!isChecked;
    if(isChecked == YES){
    _femaleCheckBox.image = [UIImage imageNamed:@"check2"];
    _maleCheckBox.image = [UIImage imageNamed:@"checkbox"];
    }
    genderString = [NSString stringWithFormat:@"2"];
}
@end
