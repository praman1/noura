//
//  ConfirmationController.m
//  Noura App
//
//  Created by Mohammad Apsar on 8/31/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ConfirmationController.h"
#import "ConfirmationCell.h"
#import "CartController.h"
#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ThankYouOrderViewController.h"
//#import "PayTabCardReaderViewController.h"
#import "HeaderAppConstant.h"

#define IS_IPAD UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad

//#import "PaymentSucessViewController.h"
@interface ConfirmationController ()

{
    NSString * addressString;
    NSMutableArray *productsArrayCount;
    NSMutableArray *productImagesArray;
    NSMutableArray *productNamesArray;
    NSMutableArray *quantityArray;
    NSMutableArray *priceArray;
    NSMutableArray *itemIdArray;
    NSString *itemIdStr;
    
    NSMutableArray *productTotalsCountArray;
    NSMutableArray *titleArray;
    NSMutableArray *amountArray;
    NSString *orderId;
    NSObject*objectUserDefaults;
    NSString *paymentString;
    NSString *telephoneStr;
    NSString *emailStr;
    NSString *city;
    NSString *countryIdStr;
    NSString* postCode;
    
    
    NSString *firstName;
    NSString *lastName;
    NSString *countryId;
    NSString *emailId;
    NSString *zipCode;
    NSString *cityName;
    NSString *state;
    NSString *mobileNo;
}

@end

@implementation ConfirmationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    
   
}

-(void)viewWillAppear:(BOOL)animated{
    
    paymentString = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"paymentType"]];
    telephoneStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"mobile"];
    emailStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"emailId"];
    city = [[NSUserDefaults standardUserDefaults]objectForKey:@"city"];
    countryIdStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"countryId"];
    postCode = [[NSUserDefaults standardUserDefaults]objectForKey:@"postCode"];
    
    NSLog(@"transaction Id %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"pt_transaction_id"]);
    NSLog(@"Response Code %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"pt_response_code"]);
    NSLog(@"Description %@", [[NSUserDefaults standardUserDefaults] objectForKey:@"pt_description"]);
    
    
    NSString * payment_ResponseCode = [[NSUserDefaults standardUserDefaults] objectForKey:@"pt_response_code"];
    // payment_ResponseCode = @"100";
    NSString * payment_description = [[NSUserDefaults standardUserDefaults] objectForKey:@"pt_description"];
    
    if (![payment_ResponseCode isEqualToString:@""]) {
        
        self.maskView.hidden = NO;
        self.statusLabel.hidden = NO;
        self.statusLabel.text = [NSString stringWithFormat:@"Status : %@",payment_description];
     } else {
        
        self.maskView.hidden = YES;
        self.statusLabel.hidden = YES;
    }
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {
    
    _placeBtn.layer.masksToBounds = false;
    _placeBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _placeBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _placeBtn.layer.shadowRadius = 3;
    _placeBtn.layer.shadowOpacity = 0.5;
    
    _shippingToLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Shipping To"];
    _yourOrderLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"YOUR ORDERS"];
    _paymentSummeryLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Payment Summary"];
    
    [_placeBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"PLACE ORDER"] forState:UIControlStateNormal];
    
    productsArrayCount = [NSMutableArray new];
    productImagesArray = [NSMutableArray new];
    productNamesArray = [NSMutableArray new];
    quantityArray = [NSMutableArray new];
    priceArray = [NSMutableArray new];
    
    productTotalsCountArray = [NSMutableArray new];
    titleArray = [NSMutableArray new];
    amountArray = [NSMutableArray new];
    
    
    
    addressString=[[NSUserDefaults standardUserDefaults]objectForKey:@"addressString"];
    
    NSLog(@"MY ADFDREESS LABLE %@",addressString);
    
    _addressLbl.text=addressString;
    
    objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    
    [self orderDetails];
    
}

#pragma mark Get Order Details Servicecall
-(void)orderDetails
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
//   http://voliveafrica.com/noura_services/services/get_orders_totals?cart_id=246&cust_id=81&store_id=1
    
    if (objectUserDefaults != nil) {
    
    NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
    
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"cartId"] forKey:@"cart_id"];
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"] forKey:@"cust_id"];
    [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
        
    [[SharedClass sharedInstance]fetchResponseforParameter:@"get_orders_totals?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
            //NSLog(@"placeOrderServiceCall printing *****");
            dispatch_async(dispatch_get_main_queue(), ^{
                
                productsArrayCount=[dataDictionary objectForKey:@"orders"];
                productTotalsCountArray=[dataDictionary objectForKey:@"totals"];
               // categoriesImagesArrayCount=[dataDictionary objectForKey:@"image"];
                
                firstName = [[dataDictionary objectForKey:@"address"]objectForKey:@"firstname"];
                lastName = [[dataDictionary objectForKey:@"address"]objectForKey:@"lastname"];
                emailId = [[dataDictionary objectForKey:@"address" ]objectForKey:@"email"];
                cityName = [[dataDictionary objectForKey:@"address"]objectForKey:@"city"];
                state = [[dataDictionary objectForKey:@"address"]objectForKey:@"region"];
                zipCode = [[dataDictionary objectForKey:@"address"]objectForKey:@"postcode"];
                countryId = [[dataDictionary objectForKey:@"address"]objectForKey:@"country_id"];
                mobileNo = [[dataDictionary objectForKey:@"address"]objectForKey:@"telephone"];
                
        if (productsArrayCount.count>0) {
                    
            for (int i=0; i<productsArrayCount.count; i++) {
                        
                [productNamesArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"name"]];
                        
                [productImagesArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"image"]];
                        
                [quantityArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"quantity"]];
                        
                [priceArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"total_price"]];
                [itemIdArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"item_id"]];
                itemIdStr = [[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"item_id"];
                
                NSLog(@"%@",productNamesArray);
                NSLog(@"%@",productImagesArray);
                
                [_orderDetailsTableView reloadData];
            }
        }
                
        if (productTotalsCountArray.count>0)
                {
                    for (int i=0; i<productTotalsCountArray.count; i++) {
                        
                        [titleArray addObject:[[[dataDictionary objectForKey:@"totals"]objectAtIndex:i]objectForKey:@"title"]];
                        [amountArray addObject:[[[dataDictionary objectForKey:@"totals"]objectAtIndex:i]objectForKey:@"amount"]];
                        
                       }
                    if (titleArray.count == 2) {
                        _subTotalLabel.text = [titleArray objectAtIndex:0];
                        _grandTotalLabel.text = [titleArray objectAtIndex:1];
                        _orderAmountLabel.text = [amountArray objectAtIndex:0];
                        _grandTotalAmountLabel.text = [amountArray objectAtIndex:1];
                    }
                    else
                    {
                        _subTotalLabel.text = [titleArray objectAtIndex:0];
                        _shippingLabel.text = [titleArray objectAtIndex:1];
                        _grandTotalLabel.text = [titleArray objectAtIndex:2];
                        
                        _orderAmountLabel.text = [amountArray objectAtIndex:0];
                        _shippingChargeLabel.text = [amountArray objectAtIndex:1];
                        _grandTotalAmountLabel.text = [amountArray objectAtIndex:2];

                    }
                   
                }
            
            });

        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    }
    else
    {
        NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
        
       // http://voliveafrica.com/noura_services/services/guest_orders_totals?cart_id=246&store_id=1
        NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
        
        [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"guestCartId"] forKey:@"cart_id"];
        
        [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
        //[cartProductPostDictionary setObject:@"1" forKey:@"store_id"];
        
        [[SharedClass sharedInstance]fetchResponseforParameter:@"guest_orders_totals?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
            
            NSLog(@"%@",dataFromJson);
            NSLog(@"%@ My Data Is",dataDictionary);
            
            if ([status isEqualToString:@"1"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    
                });
                //NSLog(@"placeOrderServiceCall printing *****");
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                    productsArrayCount=[dataDictionary objectForKey:@"orders"];
                    productTotalsCountArray=[dataDictionary objectForKey:@"totals"];
                    // categoriesImagesArrayCount=[dataDictionary objectForKey:@"image"];
                    
                    firstName = [[dataDictionary objectForKey:@"address"]objectForKey:@"firstname"];
                    lastName = [[dataDictionary objectForKey:@"address"]objectForKey:@"lastname"];
                    emailId = [[dataDictionary objectForKey:@"address" ]objectForKey:@"email"];
                    cityName = [[dataDictionary objectForKey:@"address"]objectForKey:@"city"];
                    state = [[dataDictionary objectForKey:@"address"]objectForKey:@"region"];
                    zipCode = [[dataDictionary objectForKey:@"address"]objectForKey:@"postcode"];
                    countryId = [[dataDictionary objectForKey:@"address"]objectForKey:@"country_id"];
                    mobileNo = [[dataDictionary objectForKey:@"address"]objectForKey:@"telephone"];
                    
                    
                    if (productsArrayCount.count>0) {
                        
                        for (int i=0; i<productsArrayCount.count; i++) {
                            [productNamesArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"name"]];
                            
                            [productImagesArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"image"]];
                            
                            [quantityArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"quantity"]];
                            
                            [priceArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"total_price"]];
                            
                            //[[NSUserDefaults standardUserDefaults]setObject:categoriesIdArray forKey:@"catIdArray"];
                            NSLog(@"%@",productNamesArray);
                            NSLog(@"%@",productImagesArray);
                            //NSLog(@"%@",categoriesIdArray);
                            [_orderDetailsTableView reloadData];
                        }
                    }
                    
                    if (productTotalsCountArray.count>0)
                    {
                        for (int i=0; i<productTotalsCountArray.count; i++) {
                            [titleArray addObject:[[[dataDictionary objectForKey:@"totals"]objectAtIndex:i]objectForKey:@"title"]];
                            [amountArray addObject:[[[dataDictionary objectForKey:@"totals"]objectAtIndex:i]objectForKey:@"amount"]];
                            
                        }
                        if (titleArray.count == 2) {
                            _subTotalLabel.text = [titleArray objectAtIndex:0];
                            _grandTotalLabel.text = [titleArray objectAtIndex:1];
                            _orderAmountLabel.text = [amountArray objectAtIndex:0];
                            _grandTotalAmountLabel.text = [amountArray objectAtIndex:1];
                        }
                        else
                        {
                            _subTotalLabel.text = [titleArray objectAtIndex:0];
                            _shippingLabel.text = [titleArray objectAtIndex:1];
                            _grandTotalLabel.text = [titleArray objectAtIndex:2];
                            
                            _orderAmountLabel.text = [amountArray objectAtIndex:0];
                            _shippingChargeLabel.text = [amountArray objectAtIndex:1];
                            _grandTotalAmountLabel.text = [amountArray objectAtIndex:2];
                            
                        }
                        
                    }
                    
                });
                
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // [SVProgressHUD dismiss];
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                });
            }
            
        }];

        
        
        
    }
    
    
}

#pragma mark Tableview Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return productNamesArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ConfirmationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"confirmation"];
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[productImagesArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@""]];
    cell.productNameLabel.text = [productNamesArray objectAtIndex:indexPath.row];
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%@",[priceArray objectAtIndex:indexPath.row]];
    cell.quantityLabel.text = [NSString stringWithFormat:@"%@",[quantityArray objectAtIndex:indexPath.row]];
    return cell;
}

    
//-(void)loadPaymentController {
//        
//        
//   // NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
//    
//    
//    NSString * language;
//    
//    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
//    
//    if(selectedLanguage == ARABIC_LANGUAGE) {
//        
//        language = @"ar";
//        
//    } else {
//        
//        language = @"en";
//        
//    }
//
//    
//        
//        // Present Payment ViewController
//        
//        PayTabCardReaderViewController *view;
//        if (IS_IPAD) {
//            view = [[PayTabCardReaderViewController alloc] initWithNibName:@"PayTabCardReaderViewControllerWideScreen" bundle:nil];
//        } else {
//            view = [[PayTabCardReaderViewController alloc] initWithNibName:@"PayTabCardReaderViewController_iPhone" bundle:nil];
//        }
//    
//       // NSString *trimString = [_grandTotalAmountLabel.text stringByReplacingOccurrencesOfString:@"SAR" withString:@""];
//    
//        NSString *firstWord = [[_grandTotalAmountLabel.text componentsSeparatedByString:@" "] objectAtIndex:0];
//    
//    
//        NSString *joinedComponents = [productNamesArray componentsJoinedByString:@","];
//    
//        view.tag_amount = firstWord;
//        view.tag_title = joinedComponents;
//        view.tag_currency = @"SAR";
//        view.sdklanguage = language;
//       // view.tag_tax = tag_tax;
//    
//    
//
//    
//        view.shipping_address = self.addressLbl.text;
//        view.shipping_city = cityName;
//        view.shiping_country = @"SAU";
//        view.shipping_state = countryId;
//        view.shipping_zip_code = zipCode;
//        
//        view.billing_address = self.addressLbl.text;
//        view.billing_city = cityName;
//        view.billing_country = @"SAU";
//        view.billing_state = countryId;
//        view.billing_zip_code = zipCode;
//        
//        view.order_id = itemIdStr;//item id
//        view.phonenum = mobileNo;
//        view.customer_email = emailId;
//        
//        view.tag_merchant_email = @"nourah.alharbi@gmail.com";
//        view.secretKey = @"7um7o0dLAuyTR0wknqoYZcNl9lN7Je5RIsZ6scS3HqXzrtUOUiNTuAfEV2LFgRwYjv5MIYQqoNZQhUEbcs5G2klYm7DDYaCiGI5p";
//    
//       // view.tag_original_assignee_code = @"SDK";
//        
//        
//        NSLog(@"tag_amount is %@",view.tag_amount);
//        NSLog(@"tag_title is %@",view.tag_title);
//        NSLog(@"tag_currency is %@",view.tag_currency);
//        NSLog(@"sdklanguage is %@",view.sdklanguage);
//        //NSLog(@"%@\n\n\n",view.tag_tax);
//        
//        NSLog(@" shipping_address is %@",view.shipping_address);
//        NSLog(@"shipping_city is %@",view.shipping_city);
//        NSLog(@"shiping_country is %@",view.shiping_country);
//        NSLog(@"shipping_state is %@",view.shipping_state);
//        NSLog(@"shipping_zip_code is %@\n\n\n",view.shipping_zip_code);
//        
//        NSLog(@"billing_address is %@",view.billing_address);
//        NSLog(@"billing_city is %@",view.billing_city);
//        NSLog(@"billing_country is %@",view.billing_country);
//        NSLog(@"billing_state is %@",view.billing_state);
//        NSLog(@"billing_zip_code is %@\n\n\n",view.billing_zip_code);
//        
//        NSLog(@"order_id is %@",view.order_id);
//        NSLog(@"phonenum is %@",view.phonenum);
//        NSLog(@"customer_email is %@",view.customer_email);
//        
//        NSLog(@" tag_merchant_email is %@",view.tag_merchant_email);
//        NSLog(@"secretKey is %@",view.secretKey);
//        //NSLog(@"%@\n\n\n",view.tag_original_assignee_code);
//        
//        [self presentViewController:view animated:YES completion:nil];
//        
//        
//    }

#pragma mark Place Order Servicecall
- (IBAction)placeOrderBtn:(id)sender {
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:@"Loading..."];
    //[self confirmationBtnClicked];
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
   // http://voliveafrica.com/noura_services/services/place_order?cart_id=246&cust_id=81&store_id=1
    
    if ([paymentString isEqualToString:@"online"]) {
        
        //[self loadPaymentController];
        
    }else{
   
    if (objectUserDefaults != nil) {
        
    NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
    
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"cartId"] forKey:@"cart_id"];
    [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"] forKey:@"cust_id"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"place_order?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"success"]];
        
        NSLog(@"%@",dataFromJson);
        orderId = [dataFromJson valueForKey:@"data"];
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
        });
            NSLog(@"my data is printing *****");
            
          
            
            // cust_Id=[[dataDictionary objectForKey:@"address"]objectForKey:@"customer_id"];
            
            
            // [[NSUserDefaults standardUserDefaults] setObject:cust_Id forKey:@"cust_Id"];
            [self performSegueWithIdentifier:@"placeOrderPush" sender:self];
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                 [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    }
    else
    {
       // http://voliveafrica.com/noura_services/services/place_order_guest?cart_id=246&store_id=1
        NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
        
        
        [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
        
        // [cartProductPostDictionary setObject:@"1" forKey:@"store_id"];
        
        [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"guestCartId"] forKey:@"cart_id"];
        
        
        [[SharedClass sharedInstance]fetchResponseforParameter:@"place_order_guest?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"success"]];
            
            NSLog(@"%@",dataFromJson);
            orderId = [dataFromJson valueForKey:@"data"];
            NSLog(@"%@ My Data Is",dataDictionary);
            
            if ([status isEqualToString:@"1"])
            {dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
                NSLog(@"my data is printing *****");
                
                
                
                // cust_Id=[[dataDictionary objectForKey:@"address"]objectForKey:@"customer_id"];
                
                
                // [[NSUserDefaults standardUserDefaults] setObject:cust_Id forKey:@"cust_Id"];
                [self performSegueWithIdentifier:@"placeOrderPush" sender:self];
                
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                });
            }
            
        }];

        
    }
}
 
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"placeOrderPush"]) {
        ThankYouOrderViewController *obj = [[ThankYouOrderViewController alloc] init];
        obj = [segue destinationViewController];
        obj.orderID = orderId;
    }
}
@end
