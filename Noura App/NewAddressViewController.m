//
//  NewAddressViewController.m
//  Noura App
//
//  Created by volive solutions on 21/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "NewAddressViewController.h"
#import "SharedClass.h"
#import "SWRevealViewController.h"

@interface NewAddressViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>

{
    BOOL isChecked;
    //NSString * status;
    NSDictionary *statusDict;
    UIPickerView *countryPickerView;
    NSMutableArray * countryArr;
    UIToolbar *toolbar1;
    NSString *  selected_Type;
    NSString * defaultString;
    SharedClass *obj;
}

@end

@implementation NewAddressViewController

- (void)viewDidLoad {
  
    [super viewDidLoad];
    
    [self loadViewWithCustomDesign];
    
    
}


#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign
{
    _addNewAddressBtn.layer.masksToBounds = false;
    _addNewAddressBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _addNewAddressBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _addNewAddressBtn.layer.shadowRadius = 3;
    _addNewAddressBtn.layer.shadowOpacity = 0.5;
    
    _firstNameTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _lastNameTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _phoneNumberTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _companyTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _address1TF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _address2TF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _cityTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _stateTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _pincodeTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _countryTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _firstNameTF.layer.borderWidth = 1.2f;
    _lastNameTF.layer.borderWidth = 1.2f;
    _phoneNumberTF.layer.borderWidth = 1.2f;
    _companyTF.layer.borderWidth = 1.2f;
    _address1TF.layer.borderWidth = 1.2f;
    _address2TF.layer.borderWidth = 1.2f;
    _cityTF.layer.borderWidth = 1.2f;
    _stateTF.layer.borderWidth = 1.2f;
    _pincodeTF.layer.borderWidth = 1.2f;
    _countryTF.layer.borderWidth = 1.2f;
    
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _firstNameTF.textAlignment = NSTextAlignmentRight;
        _lastNameTF.textAlignment = NSTextAlignmentRight;
        _phoneNumberTF.textAlignment = NSTextAlignmentRight;
        _companyTF.textAlignment = NSTextAlignmentRight;
        _address1TF.textAlignment = NSTextAlignmentRight;
        _address2TF.textAlignment = NSTextAlignmentRight;
        _cityTF.textAlignment = NSTextAlignmentRight;
        _pincodeTF.textAlignment = NSTextAlignmentRight;
        _stateTF.textAlignment = NSTextAlignmentRight;
        _countryTF.textAlignment = NSTextAlignmentRight;
        countryArr=[[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saudi Arabia"], nil];
        
    } else  {
        
        _firstNameTF.textAlignment = NSTextAlignmentLeft;
        _lastNameTF.textAlignment = NSTextAlignmentLeft;
        _phoneNumberTF.textAlignment = NSTextAlignmentLeft;
        _companyTF.textAlignment = NSTextAlignmentLeft;
        _address1TF.textAlignment = NSTextAlignmentLeft;
        _address2TF.textAlignment = NSTextAlignmentLeft;
        _cityTF.textAlignment = NSTextAlignmentLeft;
        _pincodeTF.textAlignment = NSTextAlignmentLeft;
        _stateTF.textAlignment = NSTextAlignmentLeft;
        _countryTF.textAlignment = NSTextAlignmentLeft;
        countryArr=[[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saudi Arabia"], nil];
    }
    
    _addAddressScrollView.delegate = self;
    obj = [[SharedClass alloc]init];
    
    isChecked = NO;
    
   // countryArr=[[NSMutableArray alloc]initWithObjects:@"Saudi Arabia", nil];
    
    countryPickerView = [[UIPickerView alloc] init];
    
    countryPickerView.delegate = self;
    countryPickerView.dataSource = self;
    _countryTF.inputView = countryPickerView;
    
    toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
    toolbar1.barStyle = UIBarStyleBlackOpaque;
    toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    toolbar1.backgroundColor=[UIColor whiteColor];
    UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show)];
    [doneButton1 setTintColor:[UIColor whiteColor]];
    UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel)];
    [cancelButton1 setTintColor:[UIColor whiteColor]];
    toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
    [_countryTF setInputAccessoryView:toolbar1];
    
    [_addNewAddressBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Add a New Address"] forState:UIControlStateNormal];
    
    _defaultShippingLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Use as my default shipping address"];
    
    _firstNameTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"First Name"];
    _lastNameTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Last Name"];
    _phoneNumberTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Mobile Number"];
    _companyTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Company"];
    _address1TF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"StreetAddress1"];
    _address2TF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"StreetAddress2"];
    _cityTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"City"];
    _pincodeTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Pin Code"];
    _stateTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"State"];
    _countryTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Country"];

}

#pragma  mark Textfield Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _firstNameTF) {
        _firstNameTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-40) animated:YES];
        
    }else if (textField == _lastNameTF) {
        _lastNameTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-40) animated:YES];
        
    }else if (textField == _phoneNumberTF) {
        _phoneNumberTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _companyTF) {
        _companyTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _address1TF) {
        _address1TF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _address2TF) {
        _address2TF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _cityTF) {
        _cityTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _stateTF) {
        _stateTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _pincodeTF) {
        _pincodeTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _countryTF) {
        _countryTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }

}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _firstNameTF) {
        _firstNameTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    else if (textField == _lastNameTF){
        _lastNameTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    else if (textField == _phoneNumberTF){
        _phoneNumberTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    else if (textField == _companyTF){
        _companyTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    else if (textField == _address1TF){
        _address1TF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    else if (textField == _address2TF){
        _address2TF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    else if (textField == _cityTF){
        _cityTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    else if (textField == _stateTF){
        _stateTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    else if (textField == _pincodeTF){
        _pincodeTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    else if (textField == _countryTF){
        _countryTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    [_firstNameTF resignFirstResponder];
    [_lastNameTF resignFirstResponder];
    [_phoneNumberTF resignFirstResponder];
    [_companyTF resignFirstResponder];
    [_address1TF resignFirstResponder];
    [_address2TF resignFirstResponder];
    [_cityTF resignFirstResponder];
    [_stateTF resignFirstResponder];
    [_pincodeTF resignFirstResponder];
    [_countryTF resignFirstResponder];
}

-(void)show
{
    if(selected_Type.length == 0)
    {
        _countryTF.text = [countryArr objectAtIndex:0];
        
    }  else
    {
       _countryTF.text = selected_Type;
    }

    NSLog(@"myTextValue *** %@",_countryTF.text);

    
    [_countryTF resignFirstResponder];
    
}

-(void)cancel
{
     [_countryTF resignFirstResponder];
    
}


- (IBAction)countryBtn:(id)sender {
}

- (IBAction)defaultShippingBtn:(id)sender {

    NSLog(@"defalt shipping btton pressed");
    
     isChecked=!isChecked;
    if (isChecked == YES) {
        _defaultShippingCheckImage.image=[UIImage imageNamed:@"check2"];
        defaultString = [NSString stringWithFormat:@"True"];
        
    }else
    {
        _defaultShippingCheckImage.image=[UIImage imageNamed:@"checkbox"];
        defaultString = [NSString stringWithFormat:@"False"];
    }

    
}


#pragma mark Add Address Servicecall
- (IBAction)addNewAddrBtn:(id)sender {
    
    //http://voliveafrica.com/noura_services/services/add_address?customer_id=39&email=raj@gmail.com&first_name=joh,&company=volive&last_name=J&street1=1-23-22&street2=model%20colony&city=Hyderabad&country_id=sa&postcode=500045&telephone=9797979797&is_default_shipping=FALSE
    
    if ((_firstNameTF.text.length>0 && _lastNameTF.text.length>0 && _phoneNumberTF.text.length>0 && _companyTF.text.length>0 && _address1TF.text.length>0 && _address2TF.text.length>0 && _cityTF.text.length>0 && _pincodeTF.text.length>0 && _countryTF.text.length>0)) {
    
    //NSString *url = @"https://lbcii.com/services/services/add_address";
    
    NSMutableDictionary * addAddressPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"] forKey:@"customer_id"];
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"emailId"] forKey:@"email"];
                [addAddressPostDictionary setObject:_firstNameTF.text forKey:@"first_name"];
                [addAddressPostDictionary setObject:_lastNameTF.text forKey:@"last_name"];
                [addAddressPostDictionary setObject:_phoneNumberTF.text forKey:@"telephone"];
                [addAddressPostDictionary setObject:_companyTF.text forKey:@"company"];
                [addAddressPostDictionary setObject:_address1TF.text forKey:@"street1"];
                [addAddressPostDictionary setObject:_address2TF.text forKey:@"street2"];
                [addAddressPostDictionary setObject:_cityTF.text forKey:@"city"];
                [addAddressPostDictionary setObject:_pincodeTF.text forKey:@"postcode"];
                [addAddressPostDictionary setObject:_countryTF.text forKey:@"country_id"];
                [addAddressPostDictionary setObject:defaultString forKey:@"is_default_shipping"];
    
        [[SharedClass sharedInstance]urlPerameterforPost:@"add_address" withPostDict:addAddressPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
            if (dict) {
                NSLog(@"Images from server %@", dict);
                [SVProgressHUD dismiss];
                
                NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    if ([status isEqualToString:@"1"])
                    {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                        
                    message:[[SharedClass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                        
                    UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                                           style:UIAlertActionStyleDefault
                                                                                         handler:^(UIAlertAction * _Nonnull action) {
                        
                                                                                             [self.navigationController popViewControllerAnimated:YES];
                        
                                                                                         }];
                                        [alert addAction:okButton];
                                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }
                    else
                    {
                        [SVProgressHUD dismiss];
                        [[SharedClass sharedInstance] showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                        
                    }
                });
            }
            else{
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedClass sharedInstance] showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Error"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                });
            }
        }];
    }
    else
    {
        [[SharedClass sharedInstance] alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please Enter details"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
        
    }


}

- (IBAction)backBarBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark Pickerview Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (pickerView == countryPickerView) {
        return 1;
    }
    
    return 0;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == countryPickerView) {
        return [countryArr count];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == countryPickerView) {
        selected_Type = countryArr[row];
    }
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{

    if (pickerView == countryPickerView) {
        return countryArr[row];
    }
    
    return nil;
   
}
//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
//    int sectionWidth = 300;
//    return sectionWidth;
//}



@end
