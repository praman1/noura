//
//  TabCell.h
//  Noura App
//
//  Created by volive solutions on 12/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *tabName;
@property (weak, nonatomic) IBOutlet UIView *tabView;

@end
