//
//  ResetPasswordViewController.h
//  Noura App
//
//  Created by volive solutions on 06/10/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResetPasswordViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTF;

@property (weak, nonatomic) IBOutlet UITextField *cnformPasswordTF;
@property (weak, nonatomic) IBOutlet UITextField *createPasswordTF;

- (IBAction)submitBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;
- (IBAction)backBtnClicked:(id)sender;


@end
