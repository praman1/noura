//
//  TabBarController.m
//  JOTUN
//
//  Created by volive solutions on 2/7/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "TabBarController.h"
#import "AppDelegate.h"

@interface TabBarController (){
    
    AppDelegate * tabBarAppDelegate;
}

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    tabBarAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
   
    
   // [[UITabBar appearance] setSelectedImageTintColor:[UIColor whiteColor]];
    //[[UITabBar appearance] setTintColor:[UIColor redColor]];
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];

    
    
    if ([tabBarAppDelegate.selectTabItem isEqualToString:@"0"]) {
   [[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];
        [self setSelectedIndex:0];
            } else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"1"]) {
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];
        [self setSelectedIndex:1];
        
    } else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"2"]) {
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];
        [self setSelectedIndex:2];
    }else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"3"]) {
        [[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];
        [self setSelectedIndex:3];
    }



    
  
    
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
