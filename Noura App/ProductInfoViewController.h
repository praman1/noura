//
//  ProductInfoViewController.h
//  Noura App
//
//  Created by volive solutions on 9/1/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface ProductInfoViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *productInfoScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *productInfoImageView;
- (IBAction)productShareButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *wishView;
@property (weak, nonatomic) IBOutlet UILabel *productCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *presentAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *addToCartBtn;
@property (weak, nonatomic) IBOutlet UITextView *detailsTextView;
@property (weak, nonatomic) IBOutlet UILabel *noOfRatingsLabel;
@property (weak, nonatomic) IBOutlet UITableView *productFeedbackTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *similarProductsCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
- (IBAction)viewAllButton:(id)sender;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UIImageView *colorImage;
@property (weak, nonatomic) IBOutlet UIImageView *sizeImage;

- (IBAction)addToCartClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel;

@property (weak, nonatomic) IBOutlet UIButton *colorButton;
@property (weak, nonatomic) IBOutlet UIButton *sizeButton;

@property (weak, nonatomic) IBOutlet HCSStarRatingView *reviewsRatingView;
- (IBAction)favBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *favtoWishListBtn;

@property NSMutableArray * productIdArr;

-(void)productInfoServiceCall:(NSString *)str_id;
-(void)similarProductsServiceCall:(NSString *)str_id;
@property (weak, nonatomic) IBOutlet UITextField *quantityTF;
@property (weak, nonatomic) IBOutlet UILabel *codeLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewAllBtn;
@property (weak, nonatomic) IBOutlet UILabel *similarProductsLabel;





@end
