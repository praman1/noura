//
//  WishListTableViewCell.m
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "WishListTableViewCell.h"
#import "SharedClass.h"

@implementation WishListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _addToCartBtn.layer.masksToBounds = false;
    _addToCartBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _addToCartBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _addToCartBtn.layer.shadowRadius = 3;
    _addToCartBtn.layer.shadowOpacity = 0.5;
    
    _quantityTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _quantityTF.layer.borderWidth = 1.2f;
    _quantityLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Quantity :"];
    [_addToCartBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"ADD TO CART"] forState:UIControlStateNormal];

    
    self.quantityTF.delegate=self;
    
        UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
        [self.contentView addGestureRecognizer:tap];
   
 
}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    
//    [self.quantityTF resignFirstResponder];
//    
//    
//    return YES;
//    
//}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    _quantityTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
}



-(void)textFieldDidEndEditing:(UITextField *)textField
{
    _quantityTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
      [self.quantityTF resignFirstResponder];
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)addToCartButton:(id)sender {

}

- (IBAction)deleteButton:(id)sender {

}
@end
