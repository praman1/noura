//
//  ThankYouOrderViewController.h
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThankYouOrderViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
- (IBAction)continueShoppingBtn:(id)sender;
@property (weak,nonatomic)NSString *orderID;
@property (weak, nonatomic) IBOutlet UIButton *continueShoppingBtn;
@property (weak, nonatomic) IBOutlet UILabel *thankYouLabel;

@end
