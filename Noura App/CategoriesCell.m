//
//  CategoriesCell.m
//  Noura App
//
//  Created by volive solutions on 24/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "CategoriesCell.h"

@implementation CategoriesCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _categoriesImageview.layer.cornerRadius = _categoriesImageview.frame.size.width/2;
    // Initialization code
}

//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//    
//    // Configure the view for the selected state
//}


@end
