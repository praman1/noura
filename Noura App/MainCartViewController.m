//
//  MainCartViewController.m
//  Noura App
//
//  Created by Mohammad Apsar on 17/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "MainCartViewController.h"
#import "MainCartCell.h"
#import "SWRevealViewController.h"
#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ANStepperView.h"
#import "LoginViewController.h"


@interface MainCartViewController ()

{
    NSMutableArray * cartProductImgArr;
    NSMutableArray * cartProductNameArr;
    NSMutableArray * cartProductPriceArr;
    NSMutableArray * cartProductArrayCount;
    NSMutableArray * cartProductQuantityArr;
    
    NSMutableArray * itemIdArray;
    NSMutableArray * totalPriceArray;
    
    BOOL starting;
    NSObject*objectUserDefaults;
    SharedClass *objForSharedclass;
    
    NSString * payment_method_str;
    NSString * shipping_method_str;
    NSString  * addressString;
    
    
}

@end

@implementation MainCartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadViewWithCustomDesign];
    
    

    // Do any additional setup after loading the view.
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {
    
    _placeOrderBtn.layer.masksToBounds = false;
    _placeOrderBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _placeOrderBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _placeOrderBtn.layer.shadowRadius = 3;
    _placeOrderBtn.layer.shadowOpacity = 0.5;
    [_placeOrderBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Place This Order"] forState:UIControlStateNormal];
    
    objForSharedclass = [[SharedClass alloc] init];
    starting = true;
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_sideMenu setTarget: self.revealViewController];
        [_sideMenu setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
        
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];

    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getCartProductServiceCall];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Tableview Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 300, 40)];
//    lbl.text = @"No Products in cart";
//    //lbl.textColor = [UIColor blackColor];
//    [_mainTV addSubview:lbl];
    
    if (cartProductNameArr.count >0) {
        _placeOrderBtn.hidden = NO;
      //  lbl.textColor = [UIColor clearColor];

        //lbl.hidden = YES;
        return cartProductNameArr.count;
    }else{
//        lbl.hidden = NO;
//         lbl.textColor = [UIColor blackColor];
        _placeOrderBtn.hidden = YES;
        return 0;
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MainCartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CartCell"];
    if(cell == nil)
    {
        cell = [[MainCartCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"CartCell"];
    }
    
    [cell.cartProductImage sd_setImageWithURL:[NSURL URLWithString:[cartProductImgArr objectAtIndex:indexPath.row]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
        cell.cartProductName.text = [cartProductNameArr objectAtIndex:indexPath.row];
        cell.cartProductPrice.text = [totalPriceArray objectAtIndex:indexPath.row];
    
    int stepValue = [[NSString stringWithFormat:@"%@",[cartProductQuantityArr objectAtIndex:indexPath.row]] intValue];
    
    
    cell.stepperValueLbl.minimumValue = 1;
    cell.stepperValueLbl.maximumValue = 100000000000;
    
    cell.stepperValueLbl.value = stepValue;
    [cell.removeCartProductBtn addTarget:self action:@selector(removeCartProductServiceCall:) forControlEvents:UIControlEventTouchUpInside];
    
  
    
    return cell;
}

- (IBAction)placeOrderClicked:(id)sender {
    // if (objectUserDefaults!=nil) {
    if (cartProductNameArr.count>0) {
        [self performSegueWithIdentifier:@"pushToShipping" sender:self];
    }
    // }else{
         
         //http://voliveafrica.com/noura_services/services/order_confirmation?cart_id=246&shipping_method=flatrate_flatrate&payment_method=cashondelivery&store_id=1
         
//         NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
//
//         
//         NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
//         
//         [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"cartId"] forKey:@"cart_id"];
//         [cartProductPostDictionary setObject:shipping_method_str forKey:@"shipping_method"];
//         
//         [cartProductPostDictionary setObject:payment_method_str forKey:@"payment_method"];
//         
//         [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
//         //[cartProductPostDictionary setObject:@"1" forKey:@"store_id"];
//         
//         NSLog(@"cartProductPostDictionary %@",cartProductPostDictionary);
//         
//         [[SharedClass sharedInstance]fetchResponseforParameter:@"order_confirmation?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
//             
//             NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
//             
//             NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
//             //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"success"]];
//             
//             NSLog(@"%@",dataFromJson);
//             NSLog(@"%@ My Data Is",dataDictionary);
//             
//             if ([status isEqualToString:@"1"])
//             {
//                 dispatch_async(dispatch_get_main_queue(), ^{
//                     
//                     [SVProgressHUD dismiss];
//                     
//                 });
//                 NSLog(@"my data is printing *****");
//                 
//                 addressString=[NSString stringWithFormat:@"%@ ,%@, %@, %@, %@, %@, %@,%@",
//                                [[dataDictionary objectForKey:@"address"]objectForKey:@"firstname"],
//                                [[dataDictionary objectForKey:@"address"]objectForKey:@"lastname"],
//                                [[dataDictionary objectForKey:@"address"]objectForKey:@"street"],
//                                [[dataDictionary objectForKey:@"address"]objectForKey:@"city"],
//                                [[dataDictionary objectForKey:@"address"]objectForKey:@"region"],
//                                [[dataDictionary objectForKey:@"address"]objectForKey:@"postcode"],[[dataDictionary objectForKey:@"address"]objectForKey:@"country_id"],[[dataDictionary objectForKey:@"address"]objectForKey:@"telephone"]];
//                 
//                 NSLog(@"myString is %@",addressString);
//                 
//                 // [[NSUserDefaults standardUserDefaults] setObject:addressString forKey:@"addressString"];
//                 
//                 // cust_Id=[[dataDictionary objectForKey:@"address"]objectForKey:@"customer_id"];
//                 
//                 
//                 // [[NSUserDefaults standardUserDefaults] setObject:cust_Id forKey:@"cust_Id"];
//                 
//                 
//             }else {
//                 
//                 dispatch_async(dispatch_get_main_queue(), ^{
//                     
//                     [SVProgressHUD dismiss];
//                     [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
//                 });
//             }
//             
//         }];

         
         
         
    // }

}

#pragma mark GetCartProduct Servicecall
-(void)getCartProductServiceCall
{
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
   // [objForSharedclass alertforLoading:self];
//   http://voliveafrica.com/noura_services/services/get_cart_items?customer_id=39&store_id=1
 if (objectUserDefaults!=nil) {
    
    NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];

[cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
[cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
   // [cartProductPostDictionary setObject:@"1" forKey:@"store_id"];


[[SharedClass sharedInstance]fetchResponseforParameter:@"get_cart_items?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
    
    NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
    
    NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
    NSLog(@"%@",dataFromJson);
    NSLog(@"%@ My Data Is",dataDictionary);
    
   if ([status isEqualToString:@"1"])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
        });
        NSLog(@"cart product details are *****");
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            cartProductArrayCount=[dataDictionary objectForKey:@"data"];
            
            cartProductImgArr = [NSMutableArray new];
            cartProductNameArr = [NSMutableArray new];
            cartProductPriceArr = [NSMutableArray new];
            cartProductQuantityArr=[NSMutableArray new];
            itemIdArray=[NSMutableArray new];
            totalPriceArray = [NSMutableArray new];

            
            
            if (cartProductArrayCount>0) {
                for (int i=0; i<cartProductArrayCount.count; i++) {
                    [cartProductImgArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                    [cartProductNameArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                    [cartProductPriceArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                    [cartProductQuantityArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]];
                    
                    [itemIdArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"item_id"]]];
                    [totalPriceArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"total_price"]]];
                    [[NSUserDefaults standardUserDefaults] setObject:itemIdArray forKey:@"itemID"];
                    
                                    }
               
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForSharedclass.hud hideAnimated:YES];
                if(cartProductImgArr.count>0){
                  [_mainTV reloadData];
                }else{
                [objForSharedclass alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"No products in cart"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
                }
            });
        });
    }else {
        
        dispatch_async(dispatch_get_main_queue(), ^{
           // [objForSharedclass.hud hideAnimated:YES];
            //[SVProgressHUD dismiss];
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
        });
    }
    
}];
 }
 else{
     
     dispatch_async(dispatch_get_main_queue(), ^{
         
         // http://voliveafrica.com/noura_services/services/guest_cart_items?cart_id=282&store_id=1
     NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
     
     [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"guestCartId"] forKey:@"cart_id"];
     [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
     
     
     
     [[SharedClass sharedInstance]fetchResponseforParameter:@"guest_cart_items?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
         
         NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
         
         NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
         NSLog(@"%@",dataFromJson);
         NSLog(@"%@ My Data Is",dataDictionary);
         
         if ([status isEqualToString:@"1"])
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [SVProgressHUD dismiss];
                 
             });
             NSLog(@"cart product details are *****");
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 cartProductArrayCount=[dataDictionary objectForKey:@"data"];
                 
                 cartProductImgArr = [NSMutableArray new];
                 cartProductNameArr = [NSMutableArray new];
                 cartProductPriceArr = [NSMutableArray new];
                 cartProductQuantityArr=[NSMutableArray new];
                 itemIdArray=[NSMutableArray new];
                 totalPriceArray = [NSMutableArray new];
                 
                 if (cartProductArrayCount>0) {
                     for (int i=0; i<cartProductArrayCount.count; i++) {
                         [cartProductImgArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                         [cartProductNameArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                         [cartProductPriceArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                         [cartProductQuantityArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]];
                         
                         [itemIdArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"item_id"]]];
                         [totalPriceArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"total_price"]]];
                         [[NSUserDefaults standardUserDefaults] setObject:itemIdArray forKey:@"itemID"];
                         
                     }
                     
                 }
                 dispatch_async(dispatch_get_main_queue(), ^{
                     //[objForSharedclass.hud hideAnimated:YES];
                     if(cartProductImgArr.count>0){
                         [_mainTV reloadData];
                     }else{
                         [objForSharedclass alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"No products in cart"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
                     }
                 });
             });
         }else {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 // [objForSharedclass.hud hideAnimated:YES];
                 //[SVProgressHUD dismiss];
                 [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
             });
         }
         
     }];
         
     });
 }
 }

#pragma mark RemoveCartProduct Servicecall
-(void)removeCartProductServiceCall:(id)sender
{
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //[objForSharedclass alertforLoading:self];
    if (objectUserDefaults!=nil) {
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_mainTV];
    NSIndexPath *indexPath = [_mainTV indexPathForRowAtPoint:btnPosition];
    
    //http://voliveafrica.com/noura_services/services/remove_cart_items?customer_id=39&item_id=71&store_id=1
    
    NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
    
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
        
        
   // [cartProductPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [cartProductPostDictionary setObject:[itemIdArray objectAtIndex:indexPath.row]forKey:@"item_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"remove_cart_items?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
      
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
            
            
            [cartProductQuantityArr removeObjectAtIndex:indexPath.row];
            [cartProductImgArr removeObjectAtIndex:indexPath.row];
            [cartProductPriceArr removeObjectAtIndex:indexPath.row];
            [cartProductNameArr removeObjectAtIndex:indexPath.row];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"Product successfully removed from cart******");
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product successfully removed from cart"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:nil];
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:^{
                    
                    //[self getCartProductServiceCall];
                
                }];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [_mainTV reloadData];
                });
                
            });
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForSharedclass.hud hideAnimated:YES];
               // [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    }else{
        
        
        CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_mainTV];
        NSIndexPath *indexPath = [_mainTV indexPathForRowAtPoint:btnPosition];
        
        
        //http://voliveafrica.com/noura_services/services/remove_guest_cart_items?cart_id=282&item_id=71&store_id=1
        
        NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
        
               [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
        
         [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"guestCartId"] forKey:@"cart_id"];
        
        [cartProductPostDictionary setObject:[itemIdArray objectAtIndex:indexPath.row]forKey:@"item_id"];
        
        [[SharedClass sharedInstance]fetchResponseforParameter:@"remove_cart_items?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            
            NSLog(@"%@",dataFromJson);
            NSLog(@"%@ My Data Is",dataDictionary);
            
            if ([status isEqualToString:@"1"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    
                });
                
                
                [cartProductQuantityArr removeObjectAtIndex:indexPath.row];
                [cartProductImgArr removeObjectAtIndex:indexPath.row];
                [cartProductPriceArr removeObjectAtIndex:indexPath.row];
                [cartProductNameArr removeObjectAtIndex:indexPath.row];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSLog(@"Product successfully removed from cart******");
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                                
                    message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product successfully removed from cart"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                     }];
                    [alert addAction:okButton];
                    [self presentViewController:alert animated:YES completion:^{
                        
                        //[self getCartProductServiceCall];
                        
                    }];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [_mainTV reloadData];
                    });
                    
                });
                
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[objForSharedclass.hud hideAnimated:YES];
                    // [SVProgressHUD dismiss];
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                });
            }
            
        }];

        
    }
    
}



- (IBAction)stepperViewAction:(ANStepperView *)sender {
}

- (IBAction)stepperAction:(id)sender {

    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:_mainTV];
    NSIndexPath *indexPath = [_mainTV indexPathForRowAtPoint:btnPosition];
    
    ANStepperView * stepper = (ANStepperView *)sender;
    
    NSLog(@"%f",stepper.value);
    
    NSLog(@"%@",[cartProductQuantityArr objectAtIndex:indexPath.row]);
    
    NSLog(@"%ld",(long)indexPath.row);

}

#pragma mark Update Cart Servicecall
-(void)updateCartServiceCall:(NSIndexPath*)indexPath
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/noura_services/services/update_cart_items?customer_id=39&item_id=72&product_qty=1&store_id=1
    
    
    if (objectUserDefaults!=nil) {
    NSMutableDictionary *updateCartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [updateCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    [updateCartPostDictionary setObject:[itemIdArray objectAtIndex:indexPath.row]forKey:@"item_id"];
    [updateCartPostDictionary setObject:[cartProductQuantityArr objectAtIndex:indexPath.row]forKey:@"product_qty"];
    [updateCartPostDictionary setObject:languageStr forKey:@"store_id"];
   // [updateCartPostDictionary setObject:@"1" forKey:@"store_id"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"update_cart_items?" withPostDict:updateCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"Product successfully Updated******");
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product successfully updated"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                    style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                 }];
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:^{
                    
                    
                }];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_mainTV reloadData];
                });
                
            });
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    }else{
     
        //http://voliveafrica.com/noura_services/services/update_guest_cart_items?cart_id=282&item_id=72&product_qty=1&store_id=1
        
        NSMutableDictionary *updateCartPostDictionary = [[NSMutableDictionary alloc]init];
        
       
        [updateCartPostDictionary setObject:[itemIdArray objectAtIndex:indexPath.row]forKey:@"item_id"];
        [updateCartPostDictionary setObject:[cartProductQuantityArr objectAtIndex:indexPath.row]forKey:@"product_qty"];
        [updateCartPostDictionary setObject:languageStr forKey:@"store_id"];
        
        [updateCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"guestCartId"] forKey:@"cart_id"];
        
        [[SharedClass sharedInstance]fetchResponseforParameter:@"update_cart_items?" withPostDict:updateCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            
            NSLog(@"%@",dataFromJson);
            NSLog(@"%@ My Data Is",dataDictionary);
            
            if ([status isEqualToString:@"1"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    
                });
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    NSLog(@"Product successfully Updated******");
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                                
                    message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product successfully updated"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                     }];
                    [alert addAction:okButton];
                    [self presentViewController:alert animated:YES completion:^{
                        
                        
                    }];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_mainTV reloadData];
                    });
                    
                });
                
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // [SVProgressHUD dismiss];
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                });
            }
            
        }];
    }

}

@end
