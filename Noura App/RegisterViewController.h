//
//  RegisterViewController.h
//  Noura App
//
//  Created by volive solutions on 19/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface RegisterViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTF;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTF;
- (IBAction)signUpBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *maleCheckBox;
@property (weak, nonatomic) IBOutlet UIImageView *femaleCheckBox;
- (IBAction)alreadyAMemberBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *signUpScrollview;
- (IBAction)maleButton:(id)sender;
- (IBAction)femaleButton:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *termsSwitch;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UILabel *maleLabel;
@property (weak, nonatomic) IBOutlet UILabel *femaleLabel;
@property (weak, nonatomic) IBOutlet UIButton *alreadyAMemberBtn;

@end
