//
//  PaymentController.h
//  Noura App
//
//  Created by Mohammad Apsar on 8/30/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentController : UIViewController
- (IBAction)continueConfirmBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *continueBtn;

@property (weak, nonatomic) IBOutlet UILabel *cashOnDeliveryLbl;

@property (weak, nonatomic) IBOutlet UILabel *FlatRateLbl;

@property (weak, nonatomic) IBOutlet UILabel *onlinePaymentLabel;

@property (weak, nonatomic) IBOutlet UILabel *amountLbl;
@property (weak, nonatomic) IBOutlet UIView *cashOnDeliveryView;
@property (weak, nonatomic) IBOutlet UIView *flatRateView;
@property (weak, nonatomic) IBOutlet UIButton *continueConfirmBtn;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountLabel;
@property (weak, nonatomic) IBOutlet UIImageView *CODRadioBtn;
@property (weak, nonatomic) IBOutlet UIImageView *onlinePaymentRadioBtn;
- (IBAction)COD_BTN:(id)sender;
- (IBAction)onlinePay_BTN:(id)sender;

@end
