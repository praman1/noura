//
//  HomeViewController.m
//  Noura App
//
//  Created by volive solutions on 23/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "CategoriesCell.h"
#import "ViewedProductsCell.h"
#import "HCSStarRatingView.h"
#import "ClothsListViewController.h"
#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ProductInfoViewController.h"
#import "MostViewedProductsViewController.h"
#import "ProductsViewController.h"
#import "AppDelegate.h"
@interface HomeViewController ()
{
    int z;
    NSMutableArray *categoriesNamesArrayCount;
    NSMutableArray *categoriesImagesArrayCount;
    NSMutableArray *productImagesArrayCount;
    NSMutableArray *productNamesArrayCount;
    NSMutableArray *productPriceArray;
    NSMutableArray *ratingArray;
    NSMutableArray *categoriesIdArray;
    NSMutableArray * productIdArr;
    NSMutableArray *subCatCountArray;
    AppDelegate *menuAppDelegate;
    
    NSString * productId,*subCatCountString;
    SharedClass *objForsharedclass;
}

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    menuAppDelegate.selectTabItem = @"0";
    [self loadViewWithCustomDesign];
    
    
   
    // Do any additional setup after loading the view.
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {
    
    objForsharedclass = [[SharedClass alloc] init];
    _categoriesImagesArray = [NSMutableArray new];
    _categoriesNamesArray = [NSMutableArray new];
    categoriesNamesArrayCount = [NSMutableArray new];
    categoriesImagesArrayCount = [NSMutableArray new];
    categoriesIdArray = [NSMutableArray new];
    subCatCountArray = [NSMutableArray new];
    
    _productsImagesArray = [NSMutableArray new];
    _productNamesArray = [NSMutableArray new];
    productNamesArrayCount = [NSMutableArray new];
    productImagesArrayCount = [NSMutableArray new];
    productPriceArray = [NSMutableArray new];
    ratingArray = [NSMutableArray new];
    
    productIdArr=[NSMutableArray new];
    
    [[[[self.tabBarController tabBar] items] objectAtIndex:0] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Home"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:1] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Cart"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:2] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Orders"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:3] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"WishList"]];

    
    _categoriesLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Browse By Catagories"];
    _mostViewedLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"FEATURED PRODUCTS"];
    
    [_viewAllBtn setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"View All"] forState:UIControlStateNormal];
    //_productsImages = [[NSMutableArray alloc]init];
    _bannerImagesArray = [NSMutableArray new];
    _addsImagesArray = [NSMutableArray new];
    
    _categoriesCollectionview.delegate = self;
    _productsCollectionview.delegate = self;
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_slideMenuBtn setTarget: self.revealViewController];
        [_slideMenuBtn setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
        
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    //[_categoriesCollectionview reloadData];
    [_productsCollectionview reloadData];
    
    [self bannerServiceCall];
    [self categoriesServiceCall];
    [self mostViewedProductsServiceCall];

    
    
    
}

#pragma mark Image Slide Show
-(void)imageSlideShow
{
    
    //PREVIOUS
    _bannerScrollview.delegate = self;
    
    _bannerScrollview.showsHorizontalScrollIndicator = NO;
    //    self.scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-105);
    _bannerScrollview.contentSize = CGSizeMake(self.view.frame.size.width *_bannerImagesArray.count,0);
    _pageControl.numberOfPages = _bannerImagesArray.count;
    
    int xPos = 0;
    
    
    for (int i = 0; i < [_bannerImagesArray count]; i++)
    {
        
        _bannerImageview = [[UIImageView alloc]initWithFrame:CGRectMake(xPos, 0, self.view.frame.size.width, 160)];
       // self.bannerImageview.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[_bannerImagesArray objectAtIndex:i]]]];
//        NSURL *imageURL=[NSURL URLWithString:[_bannerImagesArray objectAtIndex:i]];
//        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        [_bannerImageview sd_setImageWithURL:[NSURL URLWithString:[_bannerImagesArray objectAtIndex:i]]placeholderImage:[UIImage imageNamed:@"ad1"] completed:nil];
        //_bannerImageview.image = [UIImage imageWithData:imageData];
        [_bannerScrollview addSubview:_bannerImageview];
        xPos = _bannerImageview.frame.size.width+xPos;
        
        
    }
    
    
    if (_bannerImagesArray.count > 1 ) {
        
        [NSTimer scheduledTimerWithTimeInterval:5.0
                                         target:self
                                       selector:@selector(automaticBannerChangeMethod)
                                       userInfo:nil
                                        repeats:YES];
    }
    
    
}

#pragma mark Banner Servicecall
- (void)bannerServiceCall{
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
       // http://voliveafrica.com/noura_services/services/banners?store_id=1
        //[objForsharedclass alertforLoading:self];
        NSMutableDictionary * bannerPostDictionary = [[NSMutableDictionary alloc]init];
        [bannerPostDictionary setObject:languageStr forKey:@"store_id"];
    
     //[bannerPostDictionary setObject:@"1" forKey:@"store_id"];
        //[[SharedClass sharedInstance]showprogressfor:@"Loading.."];
        [[SharedClass sharedInstance]fetchResponseforParameter:@"banners?" withPostDict:bannerPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
           // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
            NSLog(@"%@",dataFromJson);
            NSLog(@"%@",dataDictionary);
            
            if ([status isEqualToString:@"1"] )
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // [objForSharedClass.hud hideAnimated:YES];
                    //[hud hideAnimated:YES];
                    [SVProgressHUD dismiss];
                    
                });
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[objForsharedclass.hud hideAnimated:YES];

                    NSMutableArray *bannerArray = [[dataDictionary objectForKey:@"data"]objectForKey:@"image"];
                    _bannerImagesArray = bannerArray;
                    [self imageSlideShow];
                     [self adsServiceCall];
                    //[self categoriesServiceCall];
                    
                    });
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    //[SVProgressHUD dismiss];
                   // [objForsharedclass.hud hideAnimated:YES];

                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                 });
            }
            
        }];
    
        
    }

#pragma mark Ads Servicecall
-(void)adsServiceCall
{
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/noura_services/services/get_adds?store_id=1
    NSMutableDictionary * addsPostDictionary = [[NSMutableDictionary alloc]init];
    [addsPostDictionary setObject:languageStr forKey:@"store_id"];
     //[addsPostDictionary setObject:@"1" forKey:@"store_id"];

    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"get_adds?" withPostDict:addsPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSMutableArray *addsArray = [[dataDictionary objectForKey:@"data"]objectForKey:@"images"];
                _addsImagesArray = addsArray;
                [_adImageView1 sd_setImageWithURL:[NSURL URLWithString:[_addsImagesArray objectAtIndex:0]]placeholderImage:[UIImage imageNamed:@"ad2"] completed:nil];

                [_adImageView2 sd_setImageWithURL:[NSURL URLWithString:[_addsImagesArray objectAtIndex:1]]placeholderImage:[UIImage imageNamed:@"ad3"] completed:nil];
                //[self categoriesServiceCall];
                
               // [self imageSlideShow];
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }

    }];
}

#pragma mark Categories Servicecall
-(void)categoriesServiceCall
{
   NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/noura_services/services/categories
    NSMutableDictionary * addsPostDictionary = [[NSMutableDictionary alloc]init];
    [addsPostDictionary setObject:languageStr forKey:@"store_id"];

    [[SharedClass sharedInstance]fetchResponseforParameter:@"categories?" withPostDict:addsPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
            //NSMutableArray *namesArray = [dataDictionary objectForKey:@"data"];
               // _categoriesNames = namesArray;
                categoriesNamesArrayCount=[dataDictionary objectForKey:@"data"];
                categoriesImagesArrayCount=[dataDictionary objectForKey:@"image"];
                if (categoriesNamesArrayCount.count>0) {
                    for (int i=0; i<categoriesNamesArrayCount.count; i++) {
                        [_categoriesNamesArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [_categoriesImagesArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                       [categoriesIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        [subCatCountArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"sub_cat_count"]];
                        
                       // subCatCountString = [NSString stringWithFormat:@"%@",subCatCountArray];
                        [[NSUserDefaults standardUserDefaults]setObject:categoriesIdArray forKey:@"catIdArray"];
                        NSLog(@"%@",_categoriesNamesArray);
                        NSLog(@"%@",_categoriesImagesArray);
                        NSLog(@"%@",categoriesIdArray);
                        NSLog(@"%@",subCatCountArray);
                       // NSLog(@"Sub Cat Count Sting Is %@",subCatCountArray);
                        [_categoriesCollectionview reloadData];
                        
                        
                       
                    }
                }
                
                
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
               //[SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}

#pragma mark Mostviewed Products Servicecall
-(void)mostViewedProductsServiceCall
{
   NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //http://voliveafrica.com/noura_services/services/most_visited?store_id=1
    
    NSMutableDictionary * productsPostDictionary = [[NSMutableDictionary alloc]init];
   [productsPostDictionary setObject:languageStr forKey:@"store_id"];
    
     //[productsPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"most_visited?" withPostDict:productsPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"] )
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                productNamesArrayCount=[dataDictionary objectForKey:@"data"];
                productImagesArrayCount=[dataDictionary objectForKey:@"image"];
                if (productNamesArrayCount.count>0) {
                    for (int i=0; i<productNamesArrayCount.count; i++) {
                        [_productNamesArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [_productsImagesArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [productPriceArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                        [ratingArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"rating"]];
                        [productIdArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        
                        NSLog(@" product array is %@",productIdArr);
                        
                        NSLog(@"%@",_productNamesArray);
                        NSLog(@"%@",_productsImagesArray);
                        [_productsCollectionview reloadData];
                    }
                }
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
               // [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}

-(void)automaticBannerChangeMethod
{
    
    if (z >= (self.view.frame.size.width * _bannerImagesArray.count)) {
        
        z = 0.0;
    }
    
    
    [_bannerScrollview setContentOffset:CGPointMake(z,0) animated:YES];
    
    z = z + self.view.frame.size.width;
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pageWidth = _bannerScrollview.frame.size.width;
    float fractionalPage = _bannerScrollview.contentOffset.x/pageWidth;
    NSInteger page = lround(fractionalPage);
    _pageControl.currentPage = page;
}

#pragma mark Collectionview Delegate Methods
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == _categoriesCollectionview)
    {
    
    CGFloat width = (CGFloat) (_categoriesCollectionview.frame.size.width/4);
    
    return CGSizeMake(width-10,113);
    }else if (collectionView == _productsCollectionview)
    {
        CGFloat width = (CGFloat)(_productsCollectionview.frame.size.width/3);
        return CGSizeMake(width-8, 193);
    }
    return CGSizeZero;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _categoriesCollectionview) {
        NSLog(@"%lu",(unsigned long)_categoriesNamesArray.count);
        return [_categoriesNamesArray count];
        
    }
    else if (collectionView == _productsCollectionview){
         NSLog(@"%lu",(unsigned long)_productNamesArray.count);
        return [_productNamesArray count];
    }
    else{
        return 2;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == _categoriesCollectionview) {
    CategoriesCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoriesCell" forIndexPath:indexPath];
        //cell.categoriesImageview.layer.cornerRadius = cell.categoriesImageview.frame.size.width/2;
        
        //cell.categoriesImageview.layer.borderColor = [UIColor colorWithRed:<#(CGFloat)#> green:<#(CGFloat)#> blue:<#(CGFloat)#> alpha:<#(CGFloat)#>]
        
        cell.categoriesImageview.clipsToBounds = YES;
        cell.categoriesImageview.layer.borderColor = [[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]CGColor];
        cell.categoriesImageview.layer.cornerRadius=cell.categoriesImageview.frame.size.width/2;
        [cell.categoriesImageview sd_setImageWithURL:[NSURL URLWithString:[_categoriesImagesArray objectAtIndex:indexPath.row]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
        cell.categoryNameLabel.text = [_categoriesNamesArray objectAtIndex:indexPath.row];
        
        return cell;
        
    }
    
    else if(collectionView == _productsCollectionview){
        ViewedProductsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ViewedProductsCell" forIndexPath:indexPath];
        [cell.productsImagesview sd_setImageWithURL:[NSURL URLWithString:[_productsImagesArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
        cell.productNameLabel.text = [_productNamesArray objectAtIndex:indexPath.row];
        cell.presentAmountLabel.text = [NSString stringWithFormat:@"%@ SAR", [productPriceArray objectAtIndex:indexPath.row]];
        cell.ratingView.value =[[ratingArray objectAtIndex:indexPath.row]intValue];
        
        cell.ratingView.userInteractionEnabled=NO;
        
        return cell;
        
    }
    
    return NULL;
    
    }

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == _categoriesCollectionview){
        if ([[subCatCountArray objectAtIndex:indexPath.row] intValue] ==0) {
            ProductsViewController *products = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductsViewController"];
            NSString * subCatstr=@"0";
            
            [products serviceCall:subCatstr];
            [self.navigationController pushViewController:products animated:TRUE];
            
        }else
        {
            
            ClothsListViewController *clothsList = [self.storyboard instantiateViewControllerWithIdentifier:@"ClothsListViewController"];
            [[NSUserDefaults standardUserDefaults]setObject:[categoriesIdArray objectAtIndex:indexPath.row] forKey:@"selected_cat_id"];
            [clothsList subCategoriesServiceCall:[categoriesIdArray objectAtIndex:indexPath.row]];
            [self.navigationController pushViewController:clothsList animated:YES];

         }
        
        //[self performSegueWithIdentifier:@"categoriesPush" sender:self];
     }else if (collectionView == _productsCollectionview)
    {
        
            ProductInfoViewController *productsList = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductInfoViewController"];
        
            [productsList productInfoServiceCall:[productIdArr objectAtIndex:indexPath.row]];
        
            //[self serviceCall:[self.sub_cat_idArr objectAtIndex:indexPath.row]];
            
            [self.navigationController pushViewController:productsList animated:YES];
}

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)viewAllButton:(id)sender {
    
//    MostViewedProductsViewController *  dvc=[self.storyboard instantiateViewControllerWithIdentifier:@"MostViewedProductsViewController"];
//    
//   // dvc.productIdStr=product_id;
//    
//    [self.navigationController pushViewController:dvc animated:YES];
    
}
@end
