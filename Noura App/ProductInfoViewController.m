//
//  ProductInfoViewController.m
//  Noura App
//
//  Created by volive solutions on 9/1/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ProductInfoViewController.h"
#import "ProductFeedbackTableViewCell.h"
#import "SimilarProductsCollectionViewCell.h"
#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ViewAllController.h"
#import "LoginViewController.h"

@interface ProductInfoViewController ()
{
    ProductFeedbackTableViewCell *feedbackCell;
    SimilarProductsCollectionViewCell *similarProductCell;
    
    UIPickerView *pickerview1;
    UIPickerView *pickerview2;
    UIToolbar *toolbar1;
    UIToolbar *toolbar2;
    NSMutableArray *sizeArr;
    NSMutableArray *colorArr;

    
    NSMutableArray *reviewsArrayCount;
    NSMutableArray *namesArray;
    NSMutableArray *titleArray;
    NSMutableArray *detailsArray;
    NSMutableArray *ratingsArray;
    
    NSMutableArray *productsArrayCount;
    NSMutableArray *productsImagesArray;
    NSMutableArray *productNamesArray;
    NSMutableArray *priceArray;
    NSMutableArray *ratingArray;
    NSString *product_id;
    NSString * wishlistIdStr;
    
    NSMutableArray *ArrId;
    
    BOOL wishlistClicked;
    
    SharedClass *objForsharedClass;
    NSObject*objectUserDefaults;
    
}

@end

@implementation ProductInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    [self loadViewWithCustomDesign];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self viewDidLoad];
    
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {
    
    //objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    objForsharedClass = [[SharedClass alloc] init];
    _reviewsRatingView.userInteractionEnabled=NO;
    _ratingView.userInteractionEnabled=NO;
    
    wishlistClicked = NO;
    _codeLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Product Code :"];
    _quantityLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Quantity :"];
    _detailsLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Details"];
    _similarProductsLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"SIMILAR PRODUCTS"];

    
    [_addToCartBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"ADD TO CART"] forState:UIControlStateNormal];
    [_viewAllBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"View All"] forState:UIControlStateNormal];
    
    // _sizeLabel.hidden = YES;
    // _colorLabel.hidden = YES;
    _sizeButton.hidden = YES;
    _colorButton.hidden = YES;
    _colorLabel.hidden = YES;
    _sizeLabel.hidden = YES;
    _colorImage.hidden = YES;
    _sizeImage.hidden = YES;
    _wishView.layer.cornerRadius = _wishView.frame.size.width/2;
    
    _productFeedbackTableView.estimatedRowHeight=91;
    _productFeedbackTableView.rowHeight=UITableViewAutomaticDimension;
    
    
    [self pickerLoad1];
    [self pickerLoad2];
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnClicked)];
    back.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = back;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 257, 43)];
    view.backgroundColor = [UIColor clearColor];
    
    UIImageView *imagev = [[UIImageView alloc]initWithFrame:CGRectMake(5, 2, 62, 34)];
    imagev.image = [UIImage imageNamed:@"home logo"];
    [view addSubview:imagev];
    self.navigationItem.titleView = view;
    //[self productInfoServiceCall];
    // [self similarProductsServiceCall];
    UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [self.view addGestureRecognizer:tap];
    
    _addToCartBtn.layer.masksToBounds = false;
    _addToCartBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _addToCartBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _addToCartBtn.layer.shadowRadius = 3;
    _addToCartBtn.layer.shadowOpacity = 0.5;
}

#pragma mark Textfield Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _quantityTF) {
        
        [_productInfoScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
    }
    
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.quantityTF resignFirstResponder];
    [_productInfoScrollView setContentOffset:CGPointMake(0,0) animated:YES];

}


//-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
//    [_quantityTF resignFirstResponder];
//    
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backBtnClicked{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Product Info Servicecall
-(void)productInfoServiceCall:(NSString *)str_id
{
    objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //[objForsharedClass alertforLoading:self];
    
    reviewsArrayCount = [NSMutableArray new];
    namesArray = [NSMutableArray new];
    titleArray = [NSMutableArray new];
    detailsArray = [NSMutableArray new];
    ratingsArray = [NSMutableArray new];
    
    product_id=str_id;
    
    if (objectUserDefaults!=nil) {
        
   // http://voliveafrica.com/noura_services/services/product_info?product_id=1&store_id=1
    NSMutableDictionary * productInfoPostDictionary = [[NSMutableDictionary alloc]init];
    [productInfoPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"cust_id"];
    
    [productInfoPostDictionary setObject:str_id forKey:@"product_id"];
    
     //[productInfoPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [productInfoPostDictionary setObject:languageStr forKey:@"store_id"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"product_info?" withPostDict:productInfoPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"product_info %@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForsharedClass.hud hideAnimated:YES];
                NSString *image = [[dataDictionary objectForKey:@"data"]objectForKey:@"image"];
                [_productInfoImageView sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:[UIImage imageNamed:@"img4"]];
                
                wishlistIdStr=[[[dataDictionary objectForKey:@"data"]objectForKey:@"whislist"]stringValue];
                _productNameLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"name"];
                _detailsTextView.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"description"];
                _presentAmountLabel.text = [NSString stringWithFormat:@"%@ SAR",[[dataDictionary objectForKey:@"data" ]objectForKey:@"price"]];
                _productCodeLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"sku"];
                _ratingCountLabel.text = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"rating"]];
                _noOfRatingsLabel.text = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"review_count"]];
                
               
                _ratingView.value = [[dataDictionary objectForKey:@"rating"]intValue];
                
                _reviewsRatingView.value = [[dataDictionary objectForKey:@"review_count"]intValue];
                
                
                if ([wishlistIdStr isEqualToString:@"1"]) {
                    
                    wishlistClicked =NO;
                    
                    [self.favtoWishListBtn setImage:[UIImage imageNamed:@"fav"] forState:UIControlStateNormal];
                    
                }else
                {
                    wishlistClicked =YES;
                    [self.favtoWishListBtn setImage:[UIImage imageNamed:@"fav grey"] forState:UIControlStateNormal];
                    
                }
                
            });
            
                reviewsArrayCount = [dataDictionary objectForKey:@"reviews"];
                if(reviewsArrayCount.count>0)
                {
                    for (int i=0; i<reviewsArrayCount.count; i++) {
                        [namesArray addObject:[[[dataDictionary objectForKey:@"reviews"]objectAtIndex:i]objectForKey:@"name"]];
                        [titleArray addObject:[[[dataDictionary objectForKey:@"reviews"]objectAtIndex:i]objectForKey:@"title"]];
                        [detailsArray addObject:[[[dataDictionary objectForKey:@"reviews"]objectAtIndex:i]objectForKey:@"details"]];
                        [ratingsArray addObject:[[[dataDictionary objectForKey:@"reviews"]objectAtIndex:i]objectForKey:@"rating"]];
                       // [sizeArr addObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"size"]];
                        //[colorArr addObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"size"]];
                        
                    }
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [_productFeedbackTableView reloadData];
                         
                         [self similarProductsServiceCall:str_id];
                          });
                }
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForsharedClass.hud hideAnimated:YES];
                //[SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

    }
    else
    {

        NSMutableDictionary * productInfoPostDictionary = [[NSMutableDictionary alloc]init];
        [productInfoPostDictionary setObject:@"" forKey:@"cust_id"];
        
        [productInfoPostDictionary setObject:str_id forKey:@"product_id"];
        
        //[productInfoPostDictionary setObject:@"1" forKey:@"store_id"];
        
        [productInfoPostDictionary setObject:languageStr forKey:@"store_id"];
        
        
        [[SharedClass sharedInstance]fetchResponseforParameter:@"product_info?" withPostDict:productInfoPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
            NSLog(@"product_info %@",dataFromJson);
            NSLog(@"%@",dataDictionary);
            
            if ([status isEqualToString:@"1"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    // [objForSharedClass.hud hideAnimated:YES];
                    //[hud hideAnimated:YES];
                    [SVProgressHUD dismiss];
                    
                });
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[objForsharedClass.hud hideAnimated:YES];
                    NSString *image = [[dataDictionary objectForKey:@"data"]objectForKey:@"image"];
                    [_productInfoImageView sd_setImageWithURL:[NSURL URLWithString:image] placeholderImage:[UIImage imageNamed:@"img4"]];
                    
                    wishlistIdStr=[[[dataDictionary objectForKey:@"data"]objectForKey:@"whislist"]stringValue];
                    _productNameLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"name"];
                    _detailsTextView.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"description"];
                    _presentAmountLabel.text = [[dataDictionary objectForKey:@"data" ]objectForKey:@"price"];
                    _productCodeLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"sku"];
                    _ratingCountLabel.text = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"rating"]];
                    _noOfRatingsLabel.text = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"review_count"]];
                    
                    
                    _ratingView.value = [[dataDictionary objectForKey:@"rating"]intValue];
                    
                    _reviewsRatingView.value = [[dataDictionary objectForKey:@"review_count"]intValue];
                    
                    
                    if ([wishlistIdStr isEqualToString:@"1"]) {
                        
                        wishlistClicked =NO;
                        
                        [self.favtoWishListBtn setImage:[UIImage imageNamed:@"fav"] forState:UIControlStateNormal];
                        
                    }else
                    {
                        wishlistClicked =YES;
                        [self.favtoWishListBtn setImage:[UIImage imageNamed:@"fav grey"] forState:UIControlStateNormal];
                        
                    }
                    
                });
                
                reviewsArrayCount = [dataDictionary objectForKey:@"reviews"];
                if(reviewsArrayCount.count>0)
                {
                    for (int i=0; i<reviewsArrayCount.count; i++) {
                        [namesArray addObject:[[[dataDictionary objectForKey:@"reviews"]objectAtIndex:i]objectForKey:@"name"]];
                        [titleArray addObject:[[[dataDictionary objectForKey:@"reviews"]objectAtIndex:i]objectForKey:@"title"]];
                        [detailsArray addObject:[[[dataDictionary objectForKey:@"reviews"]objectAtIndex:i]objectForKey:@"details"]];
                        [ratingsArray addObject:[[[dataDictionary objectForKey:@"reviews"]objectAtIndex:i]objectForKey:@"rating"]];
                        // [sizeArr addObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"size"]];
                        //[colorArr addObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"size"]];
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [_productFeedbackTableView reloadData];
                        
                        
                        [self similarProductsServiceCall:str_id];
                    });
                }
                
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[objForsharedClass.hud hideAnimated:YES];
                    //[SVProgressHUD dismiss];
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                });
            }
            
        }];
        
    }
}

#pragma mark Similar Products Servicecall
-(void)similarProductsServiceCall:(NSString *)str_id

{
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    productsArrayCount = [NSMutableArray new];
    productsImagesArray = [NSMutableArray new];
    productNamesArray = [NSMutableArray new];
    priceArray = [NSMutableArray new];
    ratingArray = [NSMutableArray new];
    ArrId = [[NSMutableArray alloc]init];
    
     product_id=str_id;
    //http://voliveafrica.com/noura_services/services/similar_products?product_id=13&store_id=1
    NSMutableDictionary *productsPostDictionary = [[NSMutableDictionary alloc]init];
    [productsPostDictionary setObject:str_id forKey:@"product_id"];
    
    [productsPostDictionary setObject:languageStr forKey:@"store_id"];
    
     //[productsPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"similar_products?" withPostDict:productsPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                productsArrayCount=[dataDictionary objectForKey:@"data"];
                
                if (productsArrayCount.count>0) {
                    for (int i=0; i<productsArrayCount.count; i++) {
                        [productsImagesArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [productNamesArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [priceArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                        [ratingArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"rating"]];
                        [ArrId addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        
                       // NSLog(@"%@",_clothsList);
                    }
                }
                [_similarProductsCollectionView reloadData];
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark Tableview Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [reviewsArrayCount count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   feedbackCell = [self.productFeedbackTableView dequeueReusableCellWithIdentifier:@"feedbackCell" forIndexPath:indexPath];


        feedbackCell.customerNameLabel.text = [namesArray objectAtIndex:indexPath.row];
        feedbackCell.feedbackTitleLabel.text = [titleArray objectAtIndex:indexPath.row];
        feedbackCell.feedbackInfoLabel.text = [detailsArray objectAtIndex:indexPath.row];
        feedbackCell.feedbackRatingView.value = [[ratingsArray objectAtIndex:indexPath.row]intValue];
        feedbackCell.feedbackRatingView.userInteractionEnabled=NO;

    return feedbackCell;
    
}

#pragma mark Collectionview Delegate Methods
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return productNamesArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    similarProductCell = [_similarProductsCollectionView dequeueReusableCellWithReuseIdentifier:@"similarProductCell" forIndexPath:indexPath];
    
    [similarProductCell.similarProductImageView sd_setImageWithURL:[NSURL URLWithString:[productsImagesArray objectAtIndex:indexPath.item]] placeholderImage:[UIImage imageNamed:@"img4"]];
    similarProductCell.similarProductNameLabel.text = [productNamesArray objectAtIndex:indexPath.row];
    similarProductCell.amountLabel.text = [NSString stringWithFormat:@"%@",[priceArray objectAtIndex:indexPath.row]];
    similarProductCell.similarRatingView.value = [[ratingArray objectAtIndex:indexPath.row]intValue];
    
    similarProductCell.similarRatingView.userInteractionEnabled=NO;
    
    
    return similarProductCell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [self.productInfoScrollView setContentOffset:CGPointMake(0, -self.productInfoScrollView.contentInset.top) animated:YES];
    [self productInfoServiceCall:[NSString stringWithFormat:@"%@",ArrId[indexPath.row]]];

}

- (IBAction)productShareButton:(id)sender {
}


- (IBAction)viewAllButton:(id)sender {
    
    ViewAllController *  dvc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewAllController"];
    
       dvc.productIdStr=product_id;
    
    [self.navigationController pushViewController:dvc animated:YES];

}

#pragma mark Add To Cart Servicecall
- (IBAction)addToCartClicked:(id)sender {
    objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    if (objectUserDefaults!= nil) {
        
    
    if (_quantityTF.text.length>0) {
        
       //[objForsharedClass alertforLoading:self];
    
    
    NSMutableDictionary *addToCartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addToCartPostDictionary setObject:product_id forKey:@"product_id"];
    
    [addToCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    [addToCartPostDictionary setObject:languageStr forKey:@"store_id"];
     //[addToCartPostDictionary setObject:@"1" forKey:@"store_id"];
        [addToCartPostDictionary setObject:_quantityTF.text forKey:@"product_qty"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"add_product_to_cart?" withPostDict:addToCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * wishListValue = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                //[objForsharedClass.hud hideAnimated:YES];
                
                NSLog(@"Product successfully added to Cart******");
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                                                                               message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product is added to cart successfully"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                 }];
                [alert addAction:okButton];
                
                //[wishlistArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                //[_productsCollectionView reloadData];
                [self presentViewController:alert animated:YES completion:^{
                    
                    self.tabBarController.selectedIndex= 1;
                    NSInteger tabitem = self.tabBarController.selectedIndex;
                    [[self.tabBarController.viewControllers objectAtIndex:tabitem] popToRootViewControllerAnimated:YES];
                    
                }];
                //productsCountArray=[dataDictionary objectForKey:@"data"];
                
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForsharedClass.hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

    }else{
     [SVProgressHUD dismiss];
    [objForsharedClass alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please enter product quantity"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
    
    }
}
else{
    if (_quantityTF.text.length>0) {
        
        //[objForsharedClass alertforLoading:self];
        
       // http://voliveafrica.com/noura_services/services/add_product_to_guest?cart_id=282&product_id=2&product_qty=2&store_id=1
        
        NSMutableDictionary *addToCartPostDictionary = [[NSMutableDictionary alloc]init];
        
        [addToCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"guestCartId"] forKey:@"cart_id"];
        [addToCartPostDictionary setObject:product_id forKey:@"product_id"];
          [addToCartPostDictionary setObject:_quantityTF.text forKey:@"product_qty"];
        [addToCartPostDictionary setObject:languageStr forKey:@"store_id"];
        //[addToCartPostDictionary setObject:@"1" forKey:@"store_id"];
        
        
        
        [[SharedClass sharedInstance]fetchResponseforParameter:@"add_product_to_guest?" withPostDict:addToCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            //NSString * wishListValue = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
            NSLog(@"%@",dataFromJson);
            NSLog(@"%@ My Data Is",dataDictionary);
            
            if ([status isEqualToString:@"1"])
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [SVProgressHUD dismiss];
                    //[objForsharedClass.hud hideAnimated:YES];
                    
                    NSLog(@"Product successfully added to Cart******");
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                                
                                                                                   message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product is added to cart successfully"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                       style:UIAlertActionStyleDefault
                                                                     handler:^(UIAlertAction * _Nonnull action) {
                                                                         
                                                                     }];
                    
                    [alert addAction:okButton];
                    
                    //[wishlistArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                    //[_productsCollectionView reloadData];
                    [self presentViewController:alert animated:YES completion:^{
                        
                        self.tabBarController.selectedIndex= 1;
                        NSInteger tabitem = self.tabBarController.selectedIndex;
                        [[self.tabBarController.viewControllers objectAtIndex:tabitem] popToRootViewControllerAnimated:YES];
                        
                    }];
                    //productsCountArray=[dataDictionary objectForKey:@"data"];
                    
                    
                });
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [objForsharedClass.hud hideAnimated:YES];
                    [SVProgressHUD dismiss];
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                });
            }
            
        }];
        
    }else{
        [SVProgressHUD dismiss];
        [objForsharedClass alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please enter product quantity"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
        
    }}

}

#pragma mark Pickerview Methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    
    if (pickerView == pickerview1) {
        return sizeArr.count;
    }
    else{
        return colorArr.count;
    }
}

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView == pickerview1) {
        return sizeArr[row];
    }
    else{
        return colorArr[row];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row
          forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    if (pickerView == pickerview1) {
        UILabel *channelLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+50, 0, 200, 30)];
        channelLabel.text = [NSString stringWithFormat:@"%@",[sizeArr objectAtIndex:row]];
        
        [channelLabel setFont:[UIFont fontWithName:@"Avenir Next" size:17]];
        channelLabel.adjustsFontSizeToFitWidth =YES;
        channelLabel.textAlignment = NSTextAlignmentCenter;
        channelLabel.backgroundColor = [UIColor clearColor];
        
        UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
        [tmpView insertSubview:channelLabel atIndex:0];
        //[tmpView insertSubview:channelLabel atIndex:1];
        [channelLabel setCenter:tmpView.center];
        return tmpView;
        
    }
    else{
        UILabel *channelLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.origin.x+50, 0, 200, 30)];
        channelLabel.text = [NSString stringWithFormat:@"%@",[colorArr objectAtIndex:row]];
        
        [channelLabel setFont:[UIFont fontWithName:@"Avenir Next" size:17]];
        channelLabel.adjustsFontSizeToFitWidth =YES;
        channelLabel.textAlignment = NSTextAlignmentCenter;
        channelLabel.backgroundColor = [UIColor clearColor];
        
        UIView *tmpView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 30)];
        [tmpView insertSubview:channelLabel atIndex:0];
        //[tmpView insertSubview:channelLabel atIndex:1];
        [channelLabel setCenter:tmpView.center];
        return tmpView;
        
    }
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    if (pickerView == pickerview1) {
        
        self.sizeLabel.text = [NSString stringWithFormat:@"%@",[sizeArr objectAtIndex:row]];
        
        
    }else{
        self.colorLabel.text = [NSString stringWithFormat:@"%@",[colorArr objectAtIndex:row]];
        
    }
    
    //self.nationalityTF.text = [NSString stringWithFormat:@"%@",[self.pickerViewCountryNamesArray objectAtIndex:row]];
    
}

#pragma mark PickerLoad1
-(void)pickerLoad1{
    pickerview1 = [[UIPickerView alloc]init];
    [pickerview1 setFrame:CGRectMake(0, self.view.frame.size.height-180, self.view.frame.size.width, 180)];
    pickerview1.delegate = self;
    pickerview1.backgroundColor = [UIColor whiteColor];
    toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, pickerview1.frame.origin.y-44, self.view.frame.size.width, 44)];
    toolbar1.barStyle = UIBarStyleBlackOpaque;
    toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    toolbar1.backgroundColor=[UIColor grayColor];
    UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show1)];
    [doneButton1 setTintColor:[UIColor greenColor]];
    UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel1)];
    [cancelButton1 setTintColor:[UIColor greenColor]];
    toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
    [self.view addSubview:pickerview1];
    [self.view addSubview:toolbar1];
    toolbar1.hidden = YES;
    pickerview1.hidden = YES;
}

#pragma mark PickerLoad2
-(void)pickerLoad2{
    pickerview2 = [[UIPickerView alloc]init];
    [pickerview2 setFrame:CGRectMake(0, self.view.frame.size.height-180, self.view.frame.size.width, 180)];
    pickerview2.delegate = self;
    pickerview2.backgroundColor = [UIColor whiteColor];
    toolbar2 = [[UIToolbar alloc] initWithFrame: CGRectMake(0, pickerview2.frame.origin.y-44, self.view.frame.size.width, 44)];
    toolbar2.barStyle = UIBarStyleBlackOpaque;
    toolbar2.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    toolbar2.backgroundColor=[UIColor grayColor];
    UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Done"] style: UIBarButtonItemStyleDone target: self action: @selector(show2)];
    [doneButton1 setTintColor:[UIColor greenColor]];
    UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"] style: UIBarButtonItemStyleDone target: self action: @selector(cancel2)];
    [cancelButton1 setTintColor:[UIColor greenColor]];
    toolbar2.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
    [self.view addSubview:pickerview2];
    [self.view addSubview:toolbar2];
    pickerview2.hidden = YES;
    toolbar2.hidden =YES;
}

- (IBAction)colorAction:(id)sender {
    pickerview2.hidden = NO;
    toolbar2.hidden = NO;

}

- (IBAction)sizeAction:(id)sender {
    pickerview1.hidden = NO;
    toolbar1.hidden = NO;
}

-(void)show1
{
    pickerview1.hidden = YES;
    toolbar1.hidden = YES;
    
}
-(void)cancel1
{
    pickerview1.hidden = YES;
    toolbar1.hidden = YES;
    
}
-(void)show2
{
    pickerview2.hidden = YES;
    toolbar2.hidden =YES;
    
}
-(void)cancel2
{
    pickerview2.hidden = YES;
    toolbar2.hidden =YES;
    
}

- (IBAction)favBtnAction:(id)sender {
   
    //wishlistClicked = !wishlistClicked;
  
    
    
    if ([wishlistIdStr isEqualToString:@"0"])
    
    {
        
        NSLog(@"wishlist added");
        
        [self addWishlistservice:product_id];
        
    }else{
        NSLog(@"wishlist removed");
        
        [self removewishlistService:product_id];
        
    }
    
    
}

#pragma mark Add To Wishlist Servicecall
-(void)addWishlistservice:(NSString *)str_id{
    
    objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    product_id =str_id;
//    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.productsCollectionView];
//    NSIndexPath *indexPath = [self.productsCollectionView indexPathForItemAtPoint:btnPosition];
    // http://voliveafrica.com/noura_services/services/add_or_remove_wishlist?add_or_remove=add&product_id=1&store_id=1&customer_id=39
    
    if (objectUserDefaults!=nil) {
        
    NSMutableDictionary *wishListPostDictionary = [[NSMutableDictionary alloc]init];
    [wishListPostDictionary setObject:@"add" forKey:@"add_or_remove"];
    [wishListPostDictionary setObject:str_id forKey:@"product_id"];
    [wishListPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    [wishListPostDictionary setObject:languageStr forKey:@"store_id"];
     //[wishListPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"add_or_remove_wishlist?" withPostDict:wishListPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * wishListValue = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"Product successfully added to wishlist******");
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                                                                               message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product successfully added to wishlist"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                 }];
                [alert addAction:okButton];
                
                //[wishlistArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
               // [_productsCollectionView reloadData];
                [self presentViewController:alert animated:YES completion:^{
                }];
               [self productInfoServiceCall:(NSString *)str_id];
                
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please Login to add product to wishlist"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginViewController* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                                 
                                                                 
                                                             }];
            [alert addAction:okButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });

    }
    
}

#pragma mark Remove From Wishlist Servicecall
-(void)removewishlistService:(NSString *)str_id{
    objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    product_id=str_id;
    
//    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.productsCollectionView];
//    NSIndexPath *indexPath = [self.productsCollectionView indexPathForItemAtPoint:btnPosition];
    // http://voliveafrica.com/noura_services/services/add_or_remove_wishlist?add_or_remove=add&product_id=1&store_id=1&customer_id=39
    
    if (objectUserDefaults!=nil) {
    
    NSMutableDictionary *wishListPostDictionary = [[NSMutableDictionary alloc]init];
    [wishListPostDictionary setObject:@"remove" forKey:@"add_or_remove"];
     [wishListPostDictionary setObject:str_id forKey:@"product_id"];
    
    //[wishListPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@""];
    [wishListPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    [wishListPostDictionary setObject:languageStr forKey:@"store_id"];
     //[wishListPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"add_or_remove_wishlist?" withPostDict:wishListPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * wishListValue = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"Product successfully added to wishlist******");
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                                                                               message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product successfully removed from wishlist"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                 }];
                [alert addAction:okButton];
                
              //  [wishlistArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
               // [_productsCollectionView reloadData];
                [self presentViewController:alert animated:YES completion:^{
                }];
                 [self productInfoServiceCall:(NSString *)str_id];
                
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please Login to add product to cart"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginViewController* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                                 
                                                                 
                                                             }];
            [alert addAction:okButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });

    }
 
}

@end
