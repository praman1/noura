//
//  ProductFeedbackTableViewCell.h
//  Noura App
//
//  Created by volive solutions on 9/1/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
@interface ProductFeedbackTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *feedbackTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *feedbackInfoLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *feedbackRatingView;


@end
