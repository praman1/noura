//
//  NewAddressViewController.h
//  Noura App
//
//  Created by volive solutions on 21/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface NewAddressViewController : UIViewController<UIScrollViewDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *firstNameTF;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTF;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;
@property (weak, nonatomic) IBOutlet UITextField *address1TF;
@property (weak, nonatomic) IBOutlet UITextField *address2TF;
@property (weak, nonatomic) IBOutlet UIScrollView *addAddressScrollView;


@property (weak, nonatomic) IBOutlet UITextField *cityTF;
@property (weak, nonatomic) IBOutlet UITextField *pincodeTF;
@property (weak, nonatomic) IBOutlet UITextField *stateTF;
@property (weak, nonatomic) IBOutlet UITextField *countryTF;
- (IBAction)countryBtn:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *defaultShippingCheckImage;
- (IBAction)defaultShippingBtn:(id)sender;
- (IBAction)addNewAddrBtn:(id)sender;
- (IBAction)backBarBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addNewAddressBtn;

@property (weak, nonatomic) IBOutlet UILabel *defaultShippingLabel;


@end
