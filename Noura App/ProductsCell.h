//
//  ProductsCell.h
//  Noura App
//
//  Created by volive solutions on 30/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
@interface ProductsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *presentAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldAmountLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *productRatingView;
- (IBAction)favouriteButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *favourite;
@property (weak, nonatomic) IBOutlet UIImageView *favouriteImageView;
@property (weak, nonatomic) IBOutlet UIView *cellView;

@end
