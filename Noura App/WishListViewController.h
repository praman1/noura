//
//  WishListViewController.h
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface WishListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *wishListTableview;
@property NSMutableArray *wishListImagesArray, *productNamesArray, *presentAmmountArray , *oldAmountArray;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarBtn;

@property NSString *  quantityStr;


@end
