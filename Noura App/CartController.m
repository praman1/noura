//
//  CartController.m
//  Noura App
//
//  Created by Mohammad Apsar on 8/30/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "CartController.h"
#import "PaymentController.h"
#import "ConfirmationController.h"
#import "ThankYouOrderViewController.h"
#import "SharedClass.h"
#import "HeaderAppConstant.h"
#import "AddressInCartTableViewCell.h"

@interface CartController ()<UITableViewDelegate,UITableViewDataSource>{
    PaymentController *payment;
    ConfirmationController *confirmation;
    int width;
    int height;
    UIPickerView *countryPickerView;
    NSMutableArray * countryArr;
    UIToolbar *toolbar1;
    NSString *  selected_Type;
   // NSString * status;
    NSDictionary *statusDict;
    
    NSString * label1Str;
    NSString * label2Str;
    NSString * value1Str;
    NSString * value2Str;
    NSString * totalPriceStr;
    SharedClass *objForSharedClass;
    NSString *string;
    
    //SharedClass *obj;
    NSMutableArray *ArrForAddresses;
    AddressInCartTableViewCell *cell;
    
    NSMutableArray *ArrForSelection ;
    NSMutableArray *ArrForcompany,*ArrForfirstName,*ArrForlastName,*ArrForaddress,*ArrForcity,*ArrForstate,*ArrForphone,*ArrForpincode,*ArrForcountry;
    
    
    int index;
    
    NSObject *objectUserDefaults;
    
}

@end

@implementation CartController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    
    
    objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    
//    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
//    [self.scrollView addGestureRecognizer:tap1];
    
   
    
    // Do any additional setup after loading the view.
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {
    objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    
    if (objectUserDefaults == nil) {
        [_segmentController setEnabled:NO forSegmentAtIndex:1];
    }
    else
    {
        [_segmentController setEnabled:YES forSegmentAtIndex:1];
    }

    string = @"Yes";
    _continuePayBtn.layer.masksToBounds = false;
    _continuePayBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _continuePayBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _continuePayBtn.layer.shadowRadius = 3;
    _continuePayBtn.layer.shadowOpacity = 0.5;
    
    _firstName.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _lastNameTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _companyTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _addressTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _phoneTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _cityTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _pincodeTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _stateTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    _countryTF.layer.borderColor = [[UIColor lightGrayColor]CGColor];
    
    _firstName.layer.borderWidth = 1.2f;
    _lastNameTF.layer.borderWidth = 1.2f;
    _companyTF.layer.borderWidth = 1.2f;
    _addressTF.layer.borderWidth = 1.2f;
    _phoneTF.layer.borderWidth = 1.2f;
    _cityTF.layer.borderWidth = 1.2f;
    _pincodeTF.layer.borderWidth = 1.2f;
    _stateTF.layer.borderWidth = 1.2f;
    _countryTF.layer.borderWidth = 1.2f;


    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _firstName.textAlignment = NSTextAlignmentRight;
        _lastNameTF.textAlignment = NSTextAlignmentRight;
        _companyTF.textAlignment = NSTextAlignmentRight;
        _addressTF.textAlignment = NSTextAlignmentRight;
        _phoneTF.textAlignment = NSTextAlignmentRight;
        _cityTF.textAlignment = NSTextAlignmentRight;
        _pincodeTF.textAlignment = NSTextAlignmentRight;
        _stateTF.textAlignment = NSTextAlignmentRight;
        _countryTF.textAlignment = NSTextAlignmentRight;
        countryArr=[[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saudi Arabia"], nil];
        
        
    } else  {
        
        _firstName.textAlignment = NSTextAlignmentLeft;
        _lastNameTF.textAlignment = NSTextAlignmentLeft;
        _companyTF.textAlignment = NSTextAlignmentLeft;
        _addressTF.textAlignment = NSTextAlignmentLeft;
        _phoneTF.textAlignment = NSTextAlignmentLeft;
        _cityTF.textAlignment = NSTextAlignmentLeft;
        _pincodeTF.textAlignment = NSTextAlignmentLeft;
        _stateTF.textAlignment = NSTextAlignmentLeft;
        _countryTF.textAlignment = NSTextAlignmentLeft;
        countryArr=[[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saudi Arabia"], nil];
    }

        [_segmentController setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"New Address"] forSegmentAtIndex:0];
        [_segmentController setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saved Address"] forSegmentAtIndex:1];
    
        _firstName.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"First Name"];
        _lastNameTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Last Name"];
        _companyTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Company"];
        _addressTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"StreetAddress1"];
        _phoneTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Mobile Number"];
        _cityTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"City"];
        _pincodeTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Pin Code"];
        _stateTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"State"];
        _countryTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Country"];

        [_continuePayBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"CONTINUE TO PAYMENT"] forState:UIControlStateNormal];
    
        _shippingLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Shipping"];
        _paymentLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Payment"];
        _confirmationLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Confirmation"];
        _shippingAddressLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Shipping Address"];
   
        _savedAddressTV.estimatedRowHeight = 500;
        _savedAddressTV.rowHeight = UITableViewAutomaticDimension;
    
        objForSharedClass = [[SharedClass alloc] init];
    
        countryPickerView = [[UIPickerView alloc] init];
        countryPickerView.delegate = self;
        countryPickerView.dataSource = self;
        _countryTF.inputView = countryPickerView;
    
        toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
        toolbar1.barStyle = UIBarStyleBlackOpaque;
        toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        toolbar1.backgroundColor=[UIColor whiteColor];
    
        UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show)];
   
        [doneButton1 setTintColor:[UIColor whiteColor]];
    
        UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel)];
    
        [cancelButton1 setTintColor:[UIColor whiteColor]];
        toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
        [_countryTF setInputAccessoryView:toolbar1];
    
    
        width = self.view.frame.size.width;
        height = self.view.frame.size.height;
    
        payment  = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentController"];
        confirmation = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmationController"];
    
        //shipping view
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shippingBtnClicked)];
        [self.shippingViewBtn addGestureRecognizer:tap];
    
        //payment view
        UITapGestureRecognizer *paymentTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(paymentBtnClicked)];
        [self.paymentViewBtn addGestureRecognizer:paymentTap];
    
        //confirmation view
        UITapGestureRecognizer *confirmationTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(confirmationBtnClicked)];
        [self.confirmationViewBtn addGestureRecognizer:confirmationTap];
    
        [self GetSavedAddress];

}

#pragma mark - textfield delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _firstName) {
        _firstName.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-20) animated:YES];
        
    }else if (textField == _lastNameTF) {
        _lastNameTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _phoneTF) {
        _phoneTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _companyTF) {
        _companyTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _addressTF) {
        _addressTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _cityTF) {
        _cityTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _pincodeTF) {
        _pincodeTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _countryTF) {
        _countryTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _firstName) {
        _firstName.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _lastNameTF){
        _lastNameTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _phoneTF){
        _phoneTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _companyTF){
        _companyTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _addressTF){
        _addressTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _cityTF){
        _cityTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _pincodeTF){
        _pincodeTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }else if (textField == _countryTF){
        _countryTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }

    [_firstName resignFirstResponder];
    [_lastNameTF resignFirstResponder];
    [_phoneTF resignFirstResponder];
    [_companyTF resignFirstResponder];
    [_addressTF resignFirstResponder];
    [_cityTF resignFirstResponder];
    [_pincodeTF resignFirstResponder];
    [_countryTF resignFirstResponder];
    
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _firstName) {
        
        [_lastNameTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _lastNameTF) {
        
        [_phoneTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _phoneTF) {
        
        [_companyTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _companyTF) {
        
        [_addressTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _addressTF) {
        
        [_cityTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _cityTF) {
        
        [_pincodeTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _pincodeTF) {
        
        [_countryTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _countryTF) {
        
        [_countryTF resignFirstResponder];
        
        returnValue = YES;
        
    }
    
    return returnValue;
}


#pragma mark - tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ArrForAddresses.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",ArrForAddresses[indexPath.row]]] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    cell.AddressLabel.attributedText = attrStr;
    
    if ([ArrForSelection[indexPath.row] isEqualToString:@"0"]) {
       // [ArrForSelection replaceObjectAtIndex:indexPath.row withObject:@"1"];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }else{
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        //[ArrForSelection replaceObjectAtIndex:indexPath.row withObject:@"0"];
    }
    
   // cell.AddressLabel.text = [NSString stringWithFormat:@"%@",ArrForAddresses[indexPath.row]];
    return cell;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ArrForSelection = [[NSMutableArray alloc] init];
    string = @"No";
    for (int a = 0; a<ArrForAddresses.count; a++) {
    
        if (a == indexPath.row) {
            
//            NSLog(@"indecpath %ld",(long)indexPath.row);
//            //[ArrForSelection replaceObjectAtIndex:indexPath.row withObject:@"1"];
//            if ([ArrForSelection[indexPath.row] isEqualToString:@"0"]) {
            
                [ArrForSelection addObject:@"1"];
//            }else{
//                
//                [ArrForSelection replaceObjectAtIndex:indexPath.row withObject:@"0"];
//            }
        }else{
            [ArrForSelection addObject:@"0"];
        
        }
    }
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[ArrForAddresses objectAtIndex:indexPath.row] forKey:@"addressString"];
    
    NSLog(@"ArrForSelection %@",ArrForSelection);
    index = (int)indexPath.row;
    [_savedAddressTV reloadData];
    //cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    
}

#pragma mark - Payment saved addresses service call
-(void)paymentWithSavedAddress{
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
//http://voliveafrica.com/noura_services/services/add_saved_address_to_cart
//cust_id,cart_id,store_id,address_id
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    
    NSString *strPost = [NSString stringWithFormat:@"cust_id=%@&cart_id=%@&store_id=%@&address_id=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"cartId"],languageStr,ArrForaddress[index]];
    
    [objForSharedClass postMethodForServicesRequest:@"add_saved_address_to_cart" :strPost completion:^(NSDictionary *dic, NSError *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [objForSharedClass.hud hideAnimated:YES];
            
        });
        NSLog(@"add_saved_address_to_cart %@",dic);
        
        NSString *str = [NSString stringWithFormat:@"%@",[dic valueForKey:@"status"]];
        if ([str isEqualToString:@"1"]) {
            
            [objForSharedClass.hud hideAnimated:YES];
        
        label1Str=[[dic valueForKey:@"payment"]valueForKey:@"label"];
        
        value1Str=[[dic valueForKey:@"payment"]valueForKey:@"value"];
        
        label2Str=[[dic valueForKey:@"shipping"]valueForKey:@"label"];
        value2Str=[[dic valueForKey:@"shipping"]valueForKey:@"value"];
        NSLog(@"value2Str %@",value2Str);
        
        totalPriceStr=[dic valueForKey:@"total"];
        
        //NSLog(@"mylable value is %@",[[statusDict objectForKey:@"payment"]objectForKey:@"label"]) ;
        
        [[NSUserDefaults standardUserDefaults] setObject:label1Str forKey:@"label1Str"];
        [[NSUserDefaults standardUserDefaults] setObject:value1Str forKey:@"value1Str"];
        [[NSUserDefaults standardUserDefaults] setObject:label2Str forKey:@"label2Str"];
        [[NSUserDefaults standardUserDefaults] setObject:value2Str forKey:@"value2Str"];
        [[NSUserDefaults standardUserDefaults] setObject:totalPriceStr forKey:@"totalPriceStr"];
            dispatch_async(dispatch_get_main_queue(), ^{

        [self paymentBtnClicked];
            });

        }
    }];
    
}

#pragma mark - Get saved addresses service call

-(void)GetSavedAddress{
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/noura_services/services/saved_addresess?user_id=39&store_id=1
    
    ArrForAddresses = [[NSMutableArray alloc] init];
    ArrForSelection = [[NSMutableArray alloc] init];
    ArrForcompany = [[NSMutableArray alloc] init];
    ArrForfirstName = [[NSMutableArray alloc] init];
    ArrForlastName = [[NSMutableArray alloc] init];
    ArrForAddresses = [[NSMutableArray alloc] init];
    ArrForaddress = [[NSMutableArray alloc] init];
    ArrForcity = [[NSMutableArray alloc] init];
    ArrForstate = [[NSMutableArray alloc] init];
    ArrForphone = [[NSMutableArray alloc] init];
    ArrForpincode = [[NSMutableArray alloc] init];
    ArrForcountry = [[NSMutableArray alloc] init];
    
    
       NSString *url = [NSString stringWithFormat:@"%@saved_addresess?user_id=%@&store_id=%@",base_URL,[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"],languageStr];
    [objForSharedClass GETList:url completion:^(NSDictionary *dic, NSError *err) {
        
        NSLog(@"saved_addresess %@",dic);
        NSArray *address = [dic valueForKey:@"data"];
        
        for (int i = 0 ; i<address.count; i++) {
            NSDictionary *addDic = address[i];
            
            [ArrForcompany addObject:[addDic valueForKey:@"company"]];
            [ArrForfirstName addObject:[addDic valueForKey:@"first_name"]];
            [ArrForlastName  addObject:[addDic valueForKey:@"last_name"]];
            //[ArrForAddresses addObject:[addDic valueForKey:@"first_name"]];
            [ArrForaddress addObject:[addDic valueForKey:@"id"]];
            [ArrForcity addObject:[addDic valueForKey:@"city"]];
            [ArrForstate addObject:[addDic valueForKey:@"first_name"]];
            [ArrForphone addObject:[addDic valueForKey:@"telephone"]];
            [ArrForpincode addObject:[addDic valueForKey:@"pincode"]];
            [ArrForcountry addObject:[addDic valueForKey:@"country"]];

            
            [ArrForAddresses addObject:[NSString stringWithFormat:@"%@,%@,%@,%@",[addDic valueForKey:@"first_name"],[addDic valueForKey:@"street"],[addDic valueForKey:@"pincode"],[addDic valueForKey:@"telephone"]]];
            [ArrForSelection addObject:@"0"];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{

        [_savedAddressTV reloadData];
        });

        NSLog(@"ArrForAddresses %@",ArrForAddresses);
        
    }];

}


-(void)show
{
    //    countryPickerView.hidden = YES;
    //    toolbar1.hidden = YES;
    if(selected_Type.length == 0)
    {
        _countryTF.text = [countryArr objectAtIndex:0];
        
    }  else
    {
        _countryTF.text = selected_Type;
    }
    
    NSLog(@"myTextValue *** %@",_countryTF.text);
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
    [_countryTF resignFirstResponder];
    
}
-(void)cancel
{
    [_countryTF resignFirstResponder];
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
}

#pragma mark Pickerview Methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (pickerView == countryPickerView) {
        return 1;
    }
    
    return 0;
}


-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == countryPickerView) {
        return [countryArr count];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == countryPickerView) {
        selected_Type = countryArr[row];
    }
    
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (pickerView == countryPickerView) {
        return countryArr[row];
    }
    
    return nil;
    
}

#pragma mark Shipping Btn Clicked
-(void)shippingBtnClicked{
    self.shippingView.hidden=NO;
    
    confirmation.view.hidden = YES;
    payment.view.hidden = YES;
    [payment.view removeFromSuperview];
    
    //bottom line
    self.shippingBtmView.backgroundColor = [UIColor colorWithRed:188.0f/255.0f green:154.0f/255.0f blue:50.0f/255.0f alpha:1.0];
    self.paymentBtmView.backgroundColor = [UIColor whiteColor];
    self.confirmationBtmView.backgroundColor = [UIColor whiteColor];
    
    //label
    self.lblShipping.textColor =[UIColor colorWithRed:188.0f/255.0f green:154.0f/255.0f blue:50.0f/255.0f alpha:1.0];
    self.lblPayment.textColor = [UIColor blackColor];
    self.lblConfirmation.textColor = [UIColor blackColor];
    //image
    self.imgShipping.image = [UIImage imageNamed:@"1"];
    self.imgPayment.image = [UIImage imageNamed:@"22"];
    self.imgConfirmation.image = [UIImage imageNamed:@"33"];
}

#pragma mark Payment Btn Clicked
-(void)paymentBtnClicked{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
    payment.view.hidden = NO;

    [payment.continueBtn addTarget:self action:@selector(continueConfirmationClicked) forControlEvents:UIControlEventTouchUpInside];
   
    self.shippingView.hidden=YES;
    confirmation.view.hidden = YES;
    payment.view.frame = CGRectMake(0, 0, width, height-150);

    [self.scrollView addSubview:payment.view];
    //bottom line
    self.paymentBtmView.backgroundColor = [UIColor colorWithRed:188.0f/255.0f green:154.0f/255.0f blue:50.0f/255.0f alpha:1.0];
    self.shippingBtmView.backgroundColor = [UIColor whiteColor];
    self.confirmationBtmView.backgroundColor = [UIColor whiteColor];
    
    //label
    self.lblPayment.textColor =[UIColor colorWithRed:188.0f/255.0f green:154.0f/255.0f blue:50.0f/255.0f alpha:1.0];
    self.lblShipping.textColor = [UIColor blackColor];
    self.lblConfirmation.textColor = [UIColor blackColor];
    //image
    self.imgShipping.image = [UIImage imageNamed:@"11"];
    self.imgPayment.image = [UIImage imageNamed:@"2"];
    self.imgConfirmation.image = [UIImage imageNamed:@"33"];
    });
    
}

#pragma mark Confirmation Btn Clicked
-(void)confirmationBtnClicked{
    
    dispatch_async(dispatch_get_main_queue(), ^{

    confirmation.view.hidden = NO;

    [confirmation.placeBtn addTarget:self action:@selector(placeOrderBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.shippingView.hidden=YES;
    payment.view.hidden = YES;
    confirmation.view.frame = CGRectMake(0, 0, width, height+20);
    
    [self.scrollView setContentSize:CGSizeMake(width,height+100)];

    [self.scrollView addSubview:confirmation.view];
    
    
    //bottom line

    self.confirmationBtmView.backgroundColor = [UIColor colorWithRed:188.0f/255.0f green:154.0f/255.0f blue:50.0f/255.0f alpha:1.0];
    self.shippingBtmView.backgroundColor = [UIColor whiteColor];
    self.paymentBtmView.backgroundColor = [UIColor whiteColor];
    
    //label
    self.lblConfirmation.textColor =[UIColor colorWithRed:188.0f/255.0f green:154.0f/255.0f blue:50.0f/255.0f alpha:1.0];
    self.lblPayment.textColor = [UIColor blackColor];
    self.lblShipping.textColor = [UIColor blackColor];
    //image
    self.imgShipping.image = [UIImage imageNamed:@"11"];
    self.imgPayment.image = [UIImage imageNamed:@"22"];
    self.imgConfirmation.image = [UIImage imageNamed:@"3"];
    });

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)placeOrderBtnClicked{
    
    ThankYouOrderViewController *thank = [self.storyboard instantiateViewControllerWithIdentifier:@"ThankYouOrderViewController"];
    
    [self.tabBarController.navigationController pushViewController:thank animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma add address to cart service call
- (void)addCartAddressServiceCall {
    
    //_imgShipping.image = [UIImage imageNamed:@"Green Check"];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    if (objectUserDefaults!=nil) {
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
   // http://voliveafrica.com/noura_services/services/add_address_to_cart?cust_id=81&company=volive&cart_id=246&fname=sai&lname=vamshi&address=hyd&city=hyd&state=telangana&mobile=1234567890&pincode=500049&country=SA&store_id=1
    if ((_firstName.text.length>0 && _lastNameTF.text.length>0 && _addressTF.text.length>0 && _cityTF.text.length>0 && _stateTF.text.length>0 && _phoneTF.text.length>0 && _pincodeTF.text.length>0 && _countryTF.text.length>0 )) {
    
    //NSString *url = @"https://lbcii.com/services/services/add_address_to_cart";
    
    NSMutableDictionary * addAddressPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"] forKey:@"cust_id"];
    [addAddressPostDictionary setObject:_companyTF.text forKey:@"company"];
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"cartId"] forKey:@"cart_id"];
    
    [addAddressPostDictionary setObject:_firstName.text forKey:@"fname"];
    [addAddressPostDictionary setObject:_lastNameTF.text forKey:@"lname"];
    [addAddressPostDictionary setObject:_addressTF.text forKey:@"address"];
    [addAddressPostDictionary setObject:_cityTF.text forKey:@"city"];
    [addAddressPostDictionary setObject:_stateTF.text forKey:@"state"];
    [addAddressPostDictionary setObject:_phoneTF.text forKey:@"mobile"];
    [addAddressPostDictionary setObject:_pincodeTF.text forKey:@"pincode"];
    [addAddressPostDictionary setObject:_countryTF.text forKey:@"country"];
    //[addAddressPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [addAddressPostDictionary setObject:languageStr forKey:@"store_id"];
    
    NSLog(@"addAddressPostDictionary %@",addAddressPostDictionary);
        
        [[SharedClass sharedInstance]urlPerameterforPost:@"add_address_to_cart" withPostDict:addAddressPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
            if (dict) {
                NSLog(@"Images from server %@", dict);
                [SVProgressHUD dismiss];
                
                NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if ([status isEqualToString:@"1"])
                    {
                        
                        NSString * addressString=[NSString stringWithFormat:@"%@ ,%@, %@, %@, %@, %@, %@,%@,%@",self.firstName.text,self.lastNameTF.text,self.phoneTF.text ,self.companyTF.text,self.addressTF.text,self.cityTF.text,self.pincodeTF.text,self.stateTF.text,self.countryTF.text];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:addressString forKey:@"addressString"];
                        
                        
                        label1Str=[[statusDict objectForKey:@"payment"]objectForKey:@"label"];
                        value1Str=[[statusDict objectForKey:@"payment"]objectForKey:@"value"];
                        label2Str=[[statusDict objectForKey:@"shipping"]objectForKey:@"label"];
                        value2Str=[[statusDict objectForKey:@"shipping"]objectForKey:@"value"];
                        NSLog(@"value2Str %@",value2Str);
                        totalPriceStr=[statusDict objectForKey:@"total"];
                        
                                    //NSLog(@"mylable value is %@",[[statusDict objectForKey:@"payment"]objectForKey:@"label"]) ;
                        
                        [[NSUserDefaults standardUserDefaults] setObject:label1Str forKey:@"label1Str"];
                        [[NSUserDefaults standardUserDefaults] setObject:value1Str forKey:@"value1Str"];
                        [[NSUserDefaults standardUserDefaults] setObject:label2Str forKey:@"label2Str"];
                        [[NSUserDefaults standardUserDefaults] setObject:value2Str forKey:@"value2Str"];
                        [[NSUserDefaults standardUserDefaults] setObject:totalPriceStr forKey:@"totalPriceStr"];
                                    
                        dispatch_async(dispatch_get_main_queue(), ^{
                        
                            [self paymentBtnClicked];
                        });

                    }
                    else
                    {
                        [SVProgressHUD dismiss];
                        [[SharedClass sharedInstance] showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                        
                    }
                });
            }
            else{
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[SharedClass sharedInstance] showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Error"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                });
            }
        }];
    }else
    {
       [objForSharedClass alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please enter details"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
    }
        
        
    }else{
        
        [_segmentController setEnabled:NO forSegmentAtIndex:1];
        
        NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
        // http://voliveafrica.com/noura_services/services/add_address_to_cart?cust_id=81&company=volive&cart_id=246&fname=sai&lname=vamshi&address=hyd&city=hyd&state=telangana&mobile=1234567890&pincode=500049&country=SA&store_id=1
        if ((_firstName.text.length>0 && _lastNameTF.text.length>0 && _addressTF.text.length>0 && _cityTF.text.length>0 && _stateTF.text.length>0 && _phoneTF.text.length>0 && _pincodeTF.text.length>0 && _countryTF.text.length>0 )) {
            
           // NSString *url = @"https://lbcii.com/services/services/add_address_to_guest_cart";
            
            NSMutableDictionary * addAddressPostDictionary = [[NSMutableDictionary alloc]init];
            
            [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"guestCartId"] forKey:@"cart_id"];
            [addAddressPostDictionary setObject:_companyTF.text forKey:@"company"];
            
            
            [addAddressPostDictionary setObject:_firstName.text forKey:@"fname"];
            [addAddressPostDictionary setObject:_lastNameTF.text forKey:@"lname"];
            [addAddressPostDictionary setObject:_addressTF.text forKey:@"address"];
            [addAddressPostDictionary setObject:_cityTF.text forKey:@"city"];
            [addAddressPostDictionary setObject:_stateTF.text forKey:@"state"];
            [addAddressPostDictionary setObject:_phoneTF.text forKey:@"mobile"];
            [addAddressPostDictionary setObject:_pincodeTF.text forKey:@"pincode"];
            [addAddressPostDictionary setObject:_countryTF.text forKey:@"country"];
            //[addAddressPostDictionary setObject:@"1" forKey:@"store_id"];
            
            [addAddressPostDictionary setObject:languageStr forKey:@"store_id"];
            
            NSLog(@"addAddressPostDictionary %@",addAddressPostDictionary);
            
            
            [[SharedClass sharedInstance]urlPerameterforPost:@"add_address_to_guest_cart" withPostDict:addAddressPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
                if (dict) {
                    NSLog(@"Images from server %@", dict);
                    [SVProgressHUD dismiss];
                    
                    NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        if ([status isEqualToString:@"1"])
                        {
                            
                        NSString * addressString=[NSString stringWithFormat:@"%@ ,%@, %@, %@, %@, %@, %@,%@,%@",self.firstName.text,self.lastNameTF.text,self.phoneTF.text ,self.companyTF.text,self.addressTF.text,self.cityTF.text,self.pincodeTF.text,self.stateTF.text,self.countryTF.text];
                            
                        [[NSUserDefaults standardUserDefaults]setObject:addressString forKey:@"addressString"];
                            
                        label1Str=[[statusDict objectForKey:@"payment"]objectForKey:@"label"];
                        value1Str=[[statusDict objectForKey:@"payment"]objectForKey:@"value"];
                        label2Str=[[statusDict objectForKey:@"shipping"]objectForKey:@"label"];
                        value2Str=[[statusDict objectForKey:@"shipping"]objectForKey:@"value"];
                        NSLog(@"value2Str %@",value2Str);
                        totalPriceStr=[statusDict objectForKey:@"total"];
                            
                        //NSLog(@"mylable value is %@",[[statusDict objectForKey:@"payment"]objectForKey:@"label"]) ;
                            
                        [[NSUserDefaults standardUserDefaults] setObject:label1Str forKey:@"label1Str"];
                        [[NSUserDefaults standardUserDefaults] setObject:value1Str forKey:@"value1Str"];
                        [[NSUserDefaults standardUserDefaults] setObject:label2Str forKey:@"label2Str"];
                        [[NSUserDefaults standardUserDefaults] setObject:value2Str forKey:@"value2Str"];
                        [[NSUserDefaults standardUserDefaults] setObject:totalPriceStr forKey:@"totalPriceStr"];
                                                
                        dispatch_async(dispatch_get_main_queue(), ^{
                                                    
                            [self paymentBtnClicked];
                            });
                        }
                        else
                        {
                            [SVProgressHUD dismiss];
                            [[SharedClass sharedInstance] showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                            
                        }
                    });
                }
                else{
                    [SVProgressHUD dismiss];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[SharedClass sharedInstance] showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Error"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                    });
                }
            }];
        }else
        {
            [objForSharedClass alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please enter details"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
        }
    }
    
}

-(void)continueConfirmationClicked{
    
    [self confirmationBtnClicked];
    
}

- (IBAction)backBtnClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continuePayBtn:(id)sender {
   // [objForSharedClass alertforLoading:self];
    
    if (self.cityTF.text.length==0) {
        
    }
    if (_segmentController.selectedSegmentIndex == 0) {
        
       // [SVProgressHUD dismiss];
        
        [self addCartAddressServiceCall];
        
    }else{
        if ([string  isEqualToString:@"No"] ) {
            //[SVProgressHUD dismiss];
            [self paymentWithSavedAddress];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"]
                                        
                                                                           message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please select an address"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                             }];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];

        }
    }
   }

- (IBAction)segmentAction:(id)sender {
    
    if(_segmentController.selectedSegmentIndex==0){
       _savedAddressTV.hidden=YES;
        _AddressView.hidden=NO;
    }
    else if(_segmentController.selectedSegmentIndex==1){
        
        [_savedAddressTV reloadData];
        _savedAddressTV.hidden=NO;
        _AddressView.hidden=YES;
        
    }
}

@end
