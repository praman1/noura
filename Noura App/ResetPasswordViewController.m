//
//  ResetPasswordViewController.m
//  Noura App
//
//  Created by volive solutions on 06/10/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "SharedClass.h"
#import "HeaderAppConstant.h"
#import "LoginViewController.h"
@interface ResetPasswordViewController (){
    SharedClass *obj;
    NSObject *objectUserDefaults;

}

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self loadViewWithCustomDesign];
    // Do any additional setup after loading the view.
    
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {

    objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    
    obj = [[SharedClass alloc] init];
    _submitBtn.layer.masksToBounds = false;
    _submitBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _submitBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _submitBtn.layer.shadowRadius = 3;
    _submitBtn.layer.shadowOpacity = 0.5;
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _createPasswordTF.textAlignment = NSTextAlignmentRight;
        _cnformPasswordTF.textAlignment = NSTextAlignmentRight;
        
    } else  {
        
        _createPasswordTF.textAlignment = NSTextAlignmentLeft;
        _cnformPasswordTF.textAlignment = NSTextAlignmentLeft;
    }
    
    _createPasswordTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"New Password"];
    _cnformPasswordTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Confirm Password"];
    
    [_submitBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Submit"] forState:UIControlStateNormal];


}

- (IBAction)submitBtnAction:(id)sender {
    
    
    
    [self ChangePassword];
    
}

#pragma mark - Reset Password Servicecall
-(void)ChangePassword{
//http://voliveafrica.com/noura_services/services/change_password?cust_id=81&newpass=654321&confirmpass=654321
    if (objectUserDefaults != nil) {
        
    
    
    if ((_cnformPasswordTF.text.length>0 )) {
        
    
    if( [_cnformPasswordTF.text isEqualToString:_createPasswordTF.text]){
    
    NSString *url  = [NSString stringWithFormat:@"%@change_password?cust_id=%@&newpass=%@&confirmpass=%@",base_URL,[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"],_createPasswordTF.text,_cnformPasswordTF.text];
    
    [obj GETList:url completion:^(NSDictionary *dict, NSError *err) {
        NSLog(@"change_password %@",dict);
        NSString *status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        
        if ([status isEqualToString:@"1"])
        {
                            
                       
            dispatch_async(dispatch_get_main_queue(), ^{
                
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]  message:[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Password changed successfully"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                     
                                                                 }];
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
                
                
            });
            
        }
        
    }];
    }else{
    _createPasswordTF.text = @"";
    _cnformPasswordTF.text = @"";
      [obj alertforMessage:self :@"cancel@1x" :@"Passwords mismatch,please enter once again" :[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
    }
    
    }else{
    [obj alertforMessage:self :@"cancel@1x" :[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Please enter password"] :[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
    
    }
    }
    else{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
            message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 //[SVProgressHUD dismiss];
                                                                 LoginViewController* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            [alert addAction:okButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
    }
    
}

#pragma mark Textfield Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _createPasswordTF) {
        _createPasswordTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _cnformPasswordTF) {
        _cnformPasswordTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == _createPasswordTF) {
        _createPasswordTF.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    }
    else if (textField == _cnformPasswordTF){
        _cnformPasswordTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }

    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    //[self dismissKeyboard];
    
    return YES;
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

