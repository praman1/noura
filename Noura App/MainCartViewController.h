//
//  MainCartViewController.h
//  Noura App
//
//  Created by Mohammad Apsar on 17/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANStepperView.h"
#import <SVProgressHUD/SVProgressHUD.h>
@interface MainCartViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideMenu;
@property (weak, nonatomic) IBOutlet UITableView *mainTV;

//- (IBAction)stepperViewAction:(ANStepperView *)sender;


- (IBAction)stepperAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *placeOrderBtn;

@end
