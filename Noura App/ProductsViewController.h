//
//  ProductsViewController.h
//  Noura App
//
//  Created by volive solutions on 28/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "HCSStarRatingView.h"
@interface ProductsViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UICollectionView *productsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *tabBarCollectionView;

@property  NSMutableArray * tabNames;
@property  NSMutableArray * tabViews;

@property NSMutableArray *imagesArr;
@property NSMutableArray *namesArr;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *productRatingView;

//-(void)productsServiceCall:(NSString*)string;
-(void)serviceCall:(NSString *)str_id;
@property NSMutableArray *sub_cat_idArr;

@property NSMutableArray *clothIDsArr;
@property NSMutableArray *clothNameArr;

@end
