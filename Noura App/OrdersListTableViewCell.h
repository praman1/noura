//
//  OrdersListTableViewCell.h
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrdersListTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *orderDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderStatusLabel;

@end
