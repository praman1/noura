//
//  ViewAllController.h
//  Noura App
//
//  Created by volive solutions on 27/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface ViewAllController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *viewAllCollectionView;
- (IBAction)barBtn:(id)sender;

@property NSString * productIdStr;

@end
