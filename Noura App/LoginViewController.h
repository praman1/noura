//
//  ViewController.h
//  Noura App
//
//  Created by volive solutions on 18/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
- (IBAction)forgotPasswordButton:(id)sender;
- (IBAction)rememberMeButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *rememberMeCheckBox;
- (IBAction)logInBtn:(id)sender;

- (IBAction)signUpButton:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;

@property UITextField * emailID;
- (IBAction)changeLanguageBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *rememberMeLabel;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn;
@property (weak, nonatomic) IBOutlet UIButton *changeLanguageBtn;
- (IBAction)guestLogin_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *guestLogin_Outlet;


@end

