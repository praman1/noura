//
//  MainCartCell.m
//  Noura App
//
//  Created by Mohammad Apsar on 17/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "MainCartCell.h"

@implementation MainCartCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
