//
//  ProfileViewController.h
//  Noura App
//
//  Created by volive solutions on 29/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface ProfileViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *profileScrollView;

@property (weak, nonatomic) IBOutlet UITextField *firstNameTF;
- (IBAction)firstNameEditBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTF;
- (IBAction)lastNameEditBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UIButton *emailEditBtn;
- (IBAction)maleCheckBtn:(id)sender;
- (IBAction)femaleCheckBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTF;
@property (weak, nonatomic) IBOutlet UITextView *addressTextView;
- (IBAction)addressEditBtn:(id)sender;
- (IBAction)addANewAddressBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *nameView;
@property (weak, nonatomic) IBOutlet UILabel *nameLetterLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *maleCheckBox;
@property (weak, nonatomic) IBOutlet UIImageView *femaleCheckBox;
- (IBAction)backBarBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBar;
- (IBAction)editProfileBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *profileEditBtn;
@property (weak, nonatomic) IBOutlet UIView *profilebackview;
@property (weak, nonatomic) IBOutlet UIView *addressBackview;
@property (weak, nonatomic) IBOutlet UIView *addAddressBackview;
@property (weak, nonatomic) IBOutlet UILabel *addressHeading;
@property (weak, nonatomic) IBOutlet UIButton *femaleBTn;
@property (weak, nonatomic) IBOutlet UIButton *MaleBtn;

@property (weak, nonatomic) IBOutlet UIButton *addNewAddressBtn;
@property (weak, nonatomic) IBOutlet UILabel *maleLabel;
@property (weak, nonatomic) IBOutlet UILabel *femaleLabel;
@property (weak, nonatomic) IBOutlet UILabel *profileHeaderLabel;

@end
