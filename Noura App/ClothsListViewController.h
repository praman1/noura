//
//  ClothsListViewController.h
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface ClothsListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *clothsListTableview;
@property NSMutableArray *clothsList;
-(void)subCategoriesServiceCall:(NSString*)string;
@end
