//
//  SharedClass.m
//  HAWLIK
//
//  Created by murali krishna on 2/6/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

#import "SharedClass.h"
#import "HeaderAppConstant.h"
//#import "SVProgressHUD.h"
#import "SVProgressHUD.h"

@implementation SharedClass

static SharedClass * sharedClassObj = nil;

+(SharedClass *)sharedInstance {
    
    if (sharedClassObj == nil) {
        sharedClassObj = [[super allocWithZone:NULL]init];
    }
    
    return sharedClassObj;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
    return sharedClassObj;
}

- (id)init
{
    self = [super init];
    
    if (self != nil)
    {
        
        
    }
    return self;
}

#pragma mark Network Checking
-(BOOL)isNetworkAvalible {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [Reachability reachabilityWithHostName:@"http://www.google.co.in/"];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable) {
        
        return NO;
    }
    else {
        
        return YES;
    }

}

#pragma mark Post Method

-(void)urlPerameterforPost:(NSString* )paramStr withPostDict:(NSDictionary* )postDict andReturnwith:(NouraPost)completionHandler {
    
    NSString *UrlStr;
    
    if ([paramStr isEqualToString:@"registration"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"add_address"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"update_profile"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    
    else if ([paramStr isEqualToString:@"add_address_to_cart"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if([paramStr isEqualToString:@"add_address_to_cart"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"add_saved_address_to_cart"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    else if ([paramStr isEqualToString:@"add_address_to_guest_cart"]){
        UrlStr=[NSString stringWithFormat:@"%@%@",base_URL,paramStr];
    }
    
    NSMutableURLRequest*urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:UrlStr]];
    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [urlRequest setHTTPShouldHandleCookies:NO];
    [urlRequest setTimeoutInterval:30];
    [urlRequest setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in postDict) {
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [postDict objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:body];
        
    }
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSession *session=[NSURLSession sharedSession];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData*  _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError *err;
        
        if (!error) {
            NSDictionary *serverDatadict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
            if (data != nil) {
                
                if ([paramStr isEqualToString:@"registration"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                
                else if ([paramStr isEqualToString:@"add_address"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                
                else if ([paramStr isEqualToString:@"update_profile"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                
                else if ([paramStr isEqualToString:@"add_address_to_cart"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"add_address_to_cart"]){
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                    
                }
                else if ([paramStr isEqualToString:@"add_saved_address_to_cart"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"add_address_to_guest_cart"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else {
                    
                    completionHandler (nil, nil, false, @"Failed");
                }
                
            }
            
            else {
                
                completionHandler (nil, nil, false, @"Failed");
            }
            
        }
        
        else {
            
            
            NSLog(@"Error is %@", error.debugDescription);
            
            completionHandler (nil, nil, false, @"Unable to fetch.");
            
        }
        
    }];
    
    [postDataTask resume];
    
}





#pragma mark Get Method
-(void)fetchResponseforParameter:(NSString *)parameterString withPostDict:(NSDictionary *)postDictionary andReturnWith:(NouraData)completionHandler
{
    
    NSString * urlString;
    
    urlString = [self modifyURLString:parameterString valuesDictionary:postDictionary];
    
    NSLog(@"%@ URL string :%@",parameterString,urlString);
    
    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    NSURLSessionConfiguration * urlSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession * urlSession = [NSURLSession sessionWithConfiguration:urlSessionConfiguration];
    
    NSURLSessionDataTask * dataTask = [urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil) {
            
            NSData * _Nullable  dataFromJson = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
             if (dataFromJson != nil) {
                 
                 completionHandler ( dataFromJson );
                 
             }else
             {
                 [self showprogressfor:@"Server problem,please try after sometime"];
                 [SVProgressHUD dismiss];
                
             }
            
        }
        else
        {
            [self showprogressfor:@"Server problem,please try after sometime"];
            [SVProgressHUD dismiss];
            
        }
        
        
        
    }];
    
    [dataTask resume];

    
    
}


-(NSString *)modifyURLString: (NSString *) urlString
      valuesDictionary: (NSDictionary *)valuesFromUser

{

    NSString * finalURLString;
                                //*******LOGIN*******//
    
    if ([urlString isEqualToString:@"login?"]) {
        
        
//http://voliveafrica.com/noura_services/services/login?email=raj@gmail.com&password=123456
        finalURLString = [NSString stringWithFormat:@"%@%@email=%@&password=%@",base_URL,urlString,[valuesFromUser objectForKey:@"email"],[valuesFromUser objectForKey:@"password"]];
        
    }
                            //*******Registration*******//
    
    else if([urlString isEqualToString:@"registration?"])
    {
       // http://voliveafrica.com/noura_services/services/registration?email=ragh@gmail.com&password=123456&store_id=1&name=raj&gender=1&mobile=1234566566
        finalURLString = [NSString stringWithFormat:@"%@%@email=%@&password=%@&store_id=%@&name=%@&gender=%@&mobile=%@",base_URL,urlString,[valuesFromUser objectForKey:@"email"],[valuesFromUser objectForKey:@"password"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"name"],[valuesFromUser objectForKey:@"gender"],[valuesFromUser objectForKey:@"mobile"]];
    }
    else if ([urlString isEqualToString:@"forget_password?"])
    {
       // http://voliveafrica.com/noura_services/services/forget_password?email=rajgopal@volivesolutions.com
        finalURLString = [NSString stringWithFormat:@"%@%@&email=%@",base_URL,urlString,[valuesFromUser objectForKey:@"email"]];
    }
                            //*******Banners*******//
    
    else if ([urlString isEqualToString:@"banners?"])
    {
       // http://voliveafrica.com/noura_services/services/banners?store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"store_id"]];
    }
                            //*******Categories*******//
    
    else if ([urlString isEqualToString:@"categories?"])
    {
        //http://voliveafrica.com/noura_services/services/categories
        finalURLString = [NSString stringWithFormat:@"%@%@store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"store_id"]];
    }
                            //*******Sub Categories*******//
    
    else if ([urlString isEqualToString:@"sub_categories?"])
    {
       // http://voliveafrica.com/noura_services/services/sub_categories?cat_id=24
        finalURLString = [NSString stringWithFormat:@"%@%@cat_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cat_id"]];
    }
                                //*******Products*******//
    
    else if ([urlString isEqualToString:@"products?"])
    {
       // http://voliveafrica.com/noura_services/services/products?cat_id=24&sub_cat_id=3&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@cat_id=%@&sub_cat_id=%@&store_id=%@&cust_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cat_id"],[valuesFromUser objectForKey:@"sub_cat_id"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"cust_id"]];
    }
                            //*******Product Info*******//
    
    else if ([urlString isEqualToString:@"product_info?"])
    {
       // http://voliveafrica.com/noura_services/services/product_info?product_id=1&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@product_id=%@&store_id=%@&cust_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"product_id"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"cust_id"]];
    }
                            //*******User Profile*******//
    
    else if ([urlString isEqualToString:@"user_profile?"])
    {
       // http://voliveafrica.com/noura_services/services/user_profile?customer_id=39&store_id=2
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
                            //*******Saved Addresess*******//
    
    else if ([urlString isEqualToString:@"saved_addresess?"])
    {
        //http://voliveafrica.com/noura_services/services/saved_addresess?user_id=39&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@user_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"user_id"],[valuesFromUser objectForKey:@"store_id"]];

    }
                                //*******User Orders*******//
    
    else if ([urlString isEqualToString:@"user_orders?"])
    {
        //http://voliveafrica.com/noura_services/services/user_orders?user_id=39&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@user_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"user_id"],[valuesFromUser objectForKey:@"store_id"]];
        finalURLString=[finalURLString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    }
                                //*******Most Visited*******//
    
    else if ([urlString isEqualToString:@"most_visited?"])
    {
        	//http://voliveafrica.com/noura_services/services/most_visited?store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"store_id"]];
    }
                                //*******Recently Added*******//
    
    else if ([urlString isEqualToString:@"recently_added?"])
    {
        //http://voliveafrica.com/noura_services/services/recently_added?cat_id=4&store_id=1
         finalURLString = [NSString stringWithFormat:@"%@%@cat_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cat_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
                                   //*******Search Product*******//
    
    else if ([urlString isEqualToString:@"search_product?"])
    {
        //http://voliveafrica.com/noura_services/services/search_product?search=product%201&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@search=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"search"],[valuesFromUser objectForKey:@"store_id"]];
    }
    
    
                                   //*******Similar Products*******//

    else if ([urlString isEqualToString:@"similar_products?"])
    {
        //http://voliveafrica.com/noura_services/services/similar_products?product_id=13&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@product_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"product_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
                                //*******Filters List*******//
    
    else if ([urlString isEqualToString:@"filters_list?"])
    {
        //http://voliveafrica.com/noura_services/services/filters_list?store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"store_id"]];
    }
                                //*******Add Address*******//
    
    else if ([urlString isEqualToString:@"add_address?"])
    {
       //http://voliveafrica.com/noura_services/services/add_address?customer_id=39&email=raj@gmail.com&first_name=joh,&last_name=J&street1=1-23-22&street2=model%20colony&city=Hyderabad&country_id=sa&postcode=500045&telephone=9797979797&is_default_shipping=FALSE
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&email=%@&first_name=%@&last_name=%@&street1=%@&street2=%@&city=%@&country_id=%@&postcode=%@&telephone=%@&is_default_shipping=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"email"],[valuesFromUser objectForKey:@"first_name"],[valuesFromUser objectForKey:@"last_name"],[valuesFromUser objectForKey:@"street1"],[valuesFromUser objectForKey:@"street2"],[valuesFromUser objectForKey:@"city"],[valuesFromUser objectForKey:@"country_id"],[valuesFromUser objectForKey:@"postcode"],[valuesFromUser objectForKey:@"telephone"],[valuesFromUser objectForKey:@"is_default_shipping"]];
    }
                            //*******Add or Remove Wishlist/ADD*******//
    
    else if ([urlString isEqualToString:@"add_or_remove_wishlist?"])
    {
        //http://voliveafrica.com/noura_services/services/add_or_remove_wishlist?add_or_remove=add&product_id=1&store_id=1&customer_id=39
        finalURLString = [NSString stringWithFormat:@"%@%@add_or_remove=%@&product_id=%@&store_id=%@&customer_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"add_or_remove"],[valuesFromUser objectForKey:@"product_id"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"customer_id"]];
    }
                            //*******Add or Remove Wishlist/REMOVE*******//
    
    else if ([urlString isEqualToString:@"add_or_remove_wishlist?"])
    {
       // http://voliveafrica.com/noura_services/services/add_or_remove_wishlist?add_or_remove=remove&product_id=1&store_id=1&customer_id=39
        finalURLString = [NSString stringWithFormat:@"%@%@add_or_remove=%@&product_id=%@&store_id=%@&customer_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"add_or_remove"],[valuesFromUser objectForKey:@"product_id"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"customer_id"]];
    }
                            //*******Cart Create*******//
    
    else if ([urlString isEqualToString:@"cart_create?"])
    {
        //http://voliveafrica.com/noura_services/services/cart_create?customer_id=39&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
                            //*******Add Product To Cart*******//
    
    else if ([urlString isEqualToString:@"add_product_to_cart?"])
    {
        //http://voliveafrica.com/noura_services/services/add_product_to_cart?customer_id=39&product_id=2&product_qty=2&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&product_id=%@&product_qty=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"product_id"],[valuesFromUser objectForKey:@"product_qty"],[valuesFromUser objectForKey:@"store_id"]];
    }
                            //*******Get Cart Items*******//
    
    else if ([urlString isEqualToString:@"get_cart_items?"])
    {
        //http://voliveafrica.com/noura_services/services/get_cart_items?customer_id=39&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
                            //*******Update Cart Items*******//
    
    else if ([urlString isEqualToString:@"update_cart_items?"])
    {
       // http://voliveafrica.com/noura_services/services/update_cart_items?customer_id=39&item_id=72&product_qty=1&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&item_id=%@&product_qty=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"item_id"],[valuesFromUser objectForKey:@"product_qty"],[valuesFromUser objectForKey:@"store_id"]];
    }
                                //*******Remove Cart Items*******//
    
    else if ([urlString isEqualToString:@"remove_cart_items?"])
    {
       // http://voliveafrica.com/noura_services/services/remove_cart_items?customer_id=39&item_id=71&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&item_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"item_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
                            //*******Get Adds*******//
    
    else if ([urlString isEqualToString:@"get_adds?"])
    {
       // http://voliveafrica.com/noura_services/services/get_adds?store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"store_id"]];
    }
                                //*******Get Wishlist*******//
    
    else if ([urlString isEqualToString:@"get_wishlist?"])
    {
        //http://voliveafrica.com/noura_services/services/get_wishlist?customer_id=39&store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
                                //*******Forget Password*******//
    else if ([urlString isEqualToString:@"forget_password?"])
    {
       // http://voliveafrica.com/noura_services/services/forget_password?email=rajgopal@volivesolutions.com
        finalURLString = [NSString stringWithFormat:@"%@%@email=%@",base_URL,urlString,[valuesFromUser objectForKey:@"email"]];
    }
                                //*******Payment Methods List*******
    else if ([urlString isEqualToString:@"payment_methods_list?"])
    {
        //http://voliveafrica.com/noura_services/services/payment_methods_list?store_id=1
        finalURLString = [NSString stringWithFormat:@"%@%@store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"store_id"]];
    }
                                //*******Filter Products*******
    else if ([urlString isEqualToString:@"filter_products?"])
    {
        //http://voliveafrica.com/noura_services/services/filter_products?sub_cat_id=10&byname=1&lowtohigh=1&hightolow=1&store_id=1&&cust_id=39
        finalURLString = [NSString stringWithFormat:@"%@%@sub_cat_id=%@&byname=%@&lowtohigh=%@&hightolow=%@&store_id=%@&cust_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"sub_cat_id"],[valuesFromUser objectForKey:@"byname"],[valuesFromUser objectForKey:@"lowtohigh"],[valuesFromUser objectForKey:@"hightolow"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"cust_id"]];
    }
    else if ([urlString isEqualToString:@"get_cart_items?"])
    {
        
        
        //http://voliveafrica.com/noura_services/services/get_cart_items?customer_id=39&store_id=1
        
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
    else if ([urlString isEqualToString:@"remove_cart_items?"])
    {
               //http://voliveafrica.com/noura_services/services/remove_cart_items?customer_id=39&item_id=71&store_id=1
        
        finalURLString = [NSString stringWithFormat:@"%@%@customer_id=%@&store_id=%@&item_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"customer_id"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"item_id"]];
    }
    else if ([urlString isEqualToString:@"order_confirmation?"])
    {
        // http://voliveafrica.com/noura_services/services/order_confirmation?cart_id=246&shipping_method=flatrate_flatrate&payment_method=cashondelivery&store_id=1
        
        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&store_id=%@&shipping_method=%@&payment_method=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"store_id"],[[NSUserDefaults standardUserDefaults]objectForKey:@"value2Str"],[[NSUserDefaults standardUserDefaults]objectForKey:@"value1Str"]];
    }
    else if ([urlString isEqualToString:@"get_orders_totals?"])
    {
        // http://voliveafrica.com/noura_services/services/get_orders_totals?cart_id=246&cust_id=81&store_id=1
        
        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&store_id=%@&cust_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"cust_id"]];
    }
    else if ([urlString isEqualToString:@"place_order?"])
    {
        
        // http://voliveafrica.com/noura_services/services/place_order?cart_id=246&cust_id=81&store_id=1
        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&store_id=%@&cust_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"cust_id"]];
    }
   
    else if ([urlString isEqualToString:@"order_information?"])
    {
        
        // http://voliveafrica.com/noura_services/services/order_information?store_id=1&order_id=100000008
        
        finalURLString = [NSString stringWithFormat:@"%@%@store_id=%@&order_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"order_id"]];
    }
    else if ([urlString isEqualToString:@"guest_cart_create?"])
    {
        
        // http://voliveafrica.com/noura_services/services/guest_cart_create?store_id=1
        
        finalURLString = [NSString stringWithFormat:@"%@%@store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"store_id"]];
    }
    else if ([urlString isEqualToString:@"add_product_to_guest?"])
    {
        
        // http://voliveafrica.com/noura_services/services/add_product_to_guest?cart_id=282&product_id=2&product_qty=2&store_id=1
        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&product_id=%@&product_qty=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"product_id"],[valuesFromUser objectForKey:@"product_qty"],[valuesFromUser objectForKey:@"store_id"]];

    }

    else if ([urlString isEqualToString:@"guest_cart_items?"])
    {
        
        // http://voliveafrica.com/noura_services/services/guest_cart_items?cart_id=282&store_id=1
        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
    else if ([urlString isEqualToString:@"remove_guest_cart_items?"])
    {
        
        //http://voliveafrica.com/noura_services/services/remove_guest_cart_items?cart_id=282&item_id=71&store_id=1
        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&store_id=%@&item_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"store_id"],[valuesFromUser objectForKey:@"item_id"]];
    }else if ([urlString isEqualToString:@"update_guest_cart_items?"])
    {
        
        //http://voliveafrica.com/noura_services/services/update_guest_cart_items?cart_id=282&item_id=72&product_qty=1&store_id=1
        
        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&item_id=%@&product_qty=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"item_id"],[valuesFromUser objectForKey:@"product_qty"],[valuesFromUser objectForKey:@"store_id"]];
    }else if ([urlString isEqualToString:@"order_confirmation?"])
    {
        
    //http://voliveafrica.com/noura_services/services/order_confirmation?cart_id=246&shipping_method=flatrate_flatrate&payment_method=cashondelivery&store_id=1
        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&item_id=%@&product_qty=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"item_id"],[valuesFromUser objectForKey:@"product_qty"],[valuesFromUser objectForKey:@"store_id"]];
    }
    else if ([urlString isEqualToString:@"guest_orders_totals?"])
    {
        
        // http://voliveafrica.com/noura_services/services/guest_orders_totals?cart_id=246&store_id=1
        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
    else if ([urlString isEqualToString:@"place_order_guest?"])
    {
        
        // http://voliveafrica.com/noura_services/services/place_order_guest?cart_id=246&store_id=1

        
        finalURLString = [NSString stringWithFormat:@"%@%@cart_id=%@&store_id=%@",base_URL,urlString,[valuesFromUser objectForKey:@"cart_id"],[valuesFromUser objectForKey:@"store_id"]];
    }
    
    
    
    
    
    
     finalURLString = [finalURLString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    
    return finalURLString;

}



-(UIImage *)drawImage:(UIImage*)profileImage withBadge:(UIImage *)badge
{
    UIGraphicsBeginImageContextWithOptions(profileImage.size, NO, 0.0f);
    [profileImage drawInRect:CGRectMake(0, 0, profileImage.size.width, profileImage.size.height)];
    [badge drawInRect:CGRectMake(19,0, badge.size.width,profileImage.size.height/2-8)];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultImage;
}



-(void)showprogressfor:(NSString *)str {
    
    [SVProgressHUD showWithStatus:str];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    
}


-(void)showprogressforSucsess:(NSString *)str {
    
    [SVProgressHUD setSuccessImage:[UIImage imageNamed:@"SuccessTick3.png"]];
    [SVProgressHUD showSuccessWithStatus:str];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD dismissWithDelay:1.0];
    
}

-(void)showAlertWithTitle: (NSString *)title withMessage: (NSString *)message onViewController: (UIViewController *)viewController completion:(void (^)(void))completionBlock;

{
    if (message == nil || [message isKindOfClass:[NSNull class]]) {
        
        title =[[SharedClass sharedInstance]languageSelectedStringForKey:@"Sorry!"] ;
        message =[[SharedClass sharedInstance]languageSelectedStringForKey:@"Server problem,please try after some time"] ;
        
    }
    
    
    UIAlertController * suggestionsAlert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"OK"] style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        completionBlock ();
        
    }];
    
    [suggestionsAlert addAction:okAction];
    
    [viewController presentViewController:suggestionsAlert animated:YES completion:nil];

    
}





#pragma mark Encode Image


- (NSString *)encodeToBase64String:(UIImage *)image
{
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

#pragma mark Decode Image

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

#pragma mark - Alert
-(void)alertforLoading:(UIViewController *)ve{
    _hud = [MBProgressHUD showHUDAddedTo:ve.view animated:YES];
    
    //_hud.mode = MBProgressHUDModeAnnularDeterminate;
    // Set the label text.
    
    _hud.label.text = NSLocalizedString(@"Loading...", @"HUD loading title");
    _hud.label.textColor = [UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0];
    
}
//-(void)dismissAlert:(UIViewController *)be{
//    _hud = [MBProgressHUD hideHUDForView:be.view animated:YES];
//}

-(void)alertforMessage:(UIViewController *)ve :(NSString *)imgname :(NSString *)text :(NSString *)buttonTitle{
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    UIColor *color = [UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0];
    [alert showCustom:ve image:[UIImage imageNamed:imgname] color:color title:[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Message"] subTitle:text closeButtonTitle:buttonTitle duration:0.0f];
}
#pragma mark - GET

-(void)GETList:(NSString *)post completion :(void (^)(NSDictionary *, NSError *))completion{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[post stringByReplacingOccurrencesOfString:@" " withString:@""]]];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@" res :%@",response);
        
        if ([data length] == 0) {
            
            
        }else{
            //NSLog(@"-data-%@",data);
            NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            _json = [NSJSONSerialization JSONObjectWithData:data
                                                    options:kNilOptions
                                                      error:nil];
            //NSLog(@"json : %@",_json);
            NSLog(@"myString : %@",myString);
            
        }
        completion(_json, error);
    }]resume];
    
    
    
    
}
#pragma mark - POST
-(void)postMethodForServicesRequest:(NSString *)serviceType :(NSString *)post completion:(void (^)(NSDictionary *, NSError *))completion{
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", (int)[postData length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",base_URL,serviceType]]];
    NSLog(@"---%@",[NSString stringWithFormat:@"%@%@",base_URL, serviceType]);
    [request setHTTPMethod:@"POST"];
    //[request setTimeoutInterval: 100];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:postData];
    
    // code here
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        _json = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions
                                                  error:nil];
        NSLog(@"_json :%@",_json);
        
        completion(_json, error);
    }]resume];
}

-(NSString*)languageSelectedStringForKey:(NSString*) key
{
    
    NSBundle *path;
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage==ENGLSIH_LANGUAGE)
        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
    
    else if (selectedLanguage==ARABIC_LANGUAGE)
    {
        
        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ar" ofType:@"lproj"]];
    }
    else{
        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
        
    }
    
    //NSBundle* languageBundle = [NSBundle bundleWithPath:path];
    NSString *str = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(key, @"LocalizedStrings", path, nil)];
    // NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
    return str;
}




@end
