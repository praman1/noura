//
//  SlideMenuTableViewCell.h
//  Noura App
//
//  Created by volive solutions on 23/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuNameLabel;



@end
