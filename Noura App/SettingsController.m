//
//  SettingsController.m
//  Together
//
//  Created by Mohammad Apsar on 7/8/17.
//  Copyright © 2017 murali krishna. All rights reserved.
//

#import "SettingsController.h"
#import "SettingsCell.h"
#import "SWRevealViewController.h"
#import "ResetPasswordViewController.h"
#import "SharedClass.h"
#import "HeaderAppConstant.h"
#import "settingsTableViewCell.h"
@interface SettingsController ()
{
//    NSInteger * ENGLSIH_LANGUAGE;
//    NSInteger * ARABIC_LANGUAGE;
    SharedClass *objForSharedClass;
}

@end

@implementation SettingsController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    //self.items_array = [[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance ]languageSelectedStringForKey:@"Language"],[[SharedClass sharedInstance ]languageSelectedStringForKey:@"Reset Password"], nil];
    
    
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {
    
    
    objForSharedClass = [[SharedClass alloc] init];
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _items_array = [[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance ]languageSelectedStringForKey:@"Reset Password"], nil];
        
    } else  {
        
        _items_array = [[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance ]languageSelectedStringForKey:@"Reset Password"], nil];
    }

    
    
    [self.setting_table reloadData];
    SWRevealViewController *revealViewController = self.revealViewController;
    
    if ( revealViewController )
    {
        [self.sideMenu setTarget: self.revealViewController];
        [self.sideMenu setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    
    
    //self.navigationItem.title =[[SharedClass sharedInstance ]languageSelectedStringForKey:@"Settings"];
    self.navigationItem.title=@"Settings";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    // Do any additional setup after loading the view.
    
    _type = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"about"]];
    
    if ([_type isEqualToString:@"about"]) {
        [self GetAboutUs];
    }else{
        
        //self.items_array = [[NSMutableArray alloc]initWithObjects:@"Language ",@" Reset Password", nil];
    }
    
    
    //    _setting_table.rowHeight = 500;
    //    _setting_table.estimatedRowHeight = UITableViewAutomaticDimension;
    
    _setting_table.estimatedRowHeight = 1000;
    _setting_table.rowHeight = UITableViewAutomaticDimension;
}

#pragma mark Aboutus Servicecall
-(void)GetAboutUs{
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//http://voliveafrica.com/noura_services/services/about_us?store_id=1
    [objForSharedClass alertforLoading:self];
    
    
    _items_array = [[NSMutableArray alloc] init];
    
    NSString *url = [NSString stringWithFormat:@"%@about_us?store_id=%@",base_URL,languageStr];
    [objForSharedClass GETList:url completion:^(NSDictionary *dict, NSError *err) {
        
        
        NSLog(@"about_us %@",[[dict valueForKey:@"data"] valueForKey:@"description"]);
        [_items_array addObject:[[dict valueForKey:@"data"] valueForKey:@"description"]];
        
         dispatch_async(dispatch_get_main_queue(), ^{
        [objForSharedClass.hud hideAnimated:YES];
        [_setting_table reloadData];
             
         });
        
    }];
    

}

#pragma mark Tableview Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.items_array.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    SettingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell"];
//    cell.labelText.text = [self.items_array objectAtIndex:indexPath.row];
    

    
    settingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingsCell" forIndexPath:indexPath];
     NSLog(@"[ self.items_array objectAtIndex:indexPath.row] %@",[ self.items_array objectAtIndex:indexPath.row]);
    
    
 NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[[NSString stringWithFormat:@"%@",[ self.items_array objectAtIndex:indexPath.row]] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    cell.contentLbl.attributedText = attrStr;
    cell.contentLbl.font = [UIFont fontWithName:@"Arial" size:15];
   
    return cell;

   }

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier: @"SettingsCell"forIndexPath:indexPath];
    
    if(![_type isEqualToString:@"about"]){
    
    if(tableView == _setting_table)
    {
        if (indexPath.row==0) {
            ResetPasswordViewController *reset = [self.storyboard    instantiateViewControllerWithIdentifier:@"reset"];
            [self.navigationController pushViewController:reset animated:YES];
        }
        
     }
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    return UITableViewAutomaticDimension;
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
