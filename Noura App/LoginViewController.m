//
//  ViewController.m
//  Noura App
//
//  Created by volive solutions on 18/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "LineTextField.h"
#import "SharedClass.h"
#import "SVProgressHUD.h"
#import "JVFloatLabeledTextView.h"

@interface LoginViewController ()
{
    BOOL isChecked;
    NSString * emailString;
    NSString * languageString;
    SharedClass *objForSharedClass;
     UIAlertController *alertController;
    
    NSString * custId;
    //NSString *selectedLanguage;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    languageString = [NSString stringWithFormat:@"1"];
    
    [[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
    
    [self loadViewWithCustomDesign];
    
}

#pragma mark LoadWithCustomDesign
-(void)loadViewWithCustomDesign {
    
    
    objForSharedClass = [[SharedClass alloc] init];
    isChecked = NO;
   // _usernameTF.delegate = self;
   // _passwordTF.delegate = self;
    
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _usernameTF.textAlignment = NSTextAlignmentRight;
        _passwordTF.textAlignment = NSTextAlignmentRight;
    
    } else  {
        
        _usernameTF.textAlignment = NSTextAlignmentLeft;
        _passwordTF.textAlignment = NSTextAlignmentLeft;
    }
    
    
    _rememberMeLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Remember me"];
    
    [_forgotPasswordBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Forgot Password ?"] forState:UIControlStateNormal];
    
    [_guestLogin_Outlet setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Guest Login"] forState:UIControlStateNormal];
    _usernameTF.layer.cornerRadius = 8;
    _usernameTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Username"];
    
    _passwordTF.layer.cornerRadius = 8;
    _passwordTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Password"];
    
    _usernameTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _passwordTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    _usernameTF.layer.borderWidth = 1.2f;
    _passwordTF.layer.borderWidth = 1.2f;
    
    _loginBtn.layer.cornerRadius = 22;
    [_loginBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"LOGIN"] forState:UIControlStateNormal];
    
    _signUpBtn.layer.cornerRadius = 23;
    [_signUpBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Sign Up With Email"] forState:UIControlStateNormal];
    _signUpBtn.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
    _signUpBtn.layer.borderWidth = 1.2f;
    [self.navigationController setNavigationBarHidden:YES];
    
    _usernameTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"saveEmail"];
    _passwordTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"savePassword"];
    
    [_changeLanguageBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Change Language"] forState:UIControlStateNormal];
    
    _loginBtn.layer.masksToBounds = false;
    _loginBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _loginBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _loginBtn.layer.shadowRadius = 3;
    _loginBtn.layer.shadowOpacity = 0.5;
    
    
    _signUpBtn.layer.masksToBounds = false;
    _signUpBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _signUpBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _signUpBtn.layer.shadowRadius = 3;
    _signUpBtn.layer.shadowOpacity = 0.5;
    
    // Do any additional setup after loading the view, typically from a nib.
    
}


#pragma mark - textfield delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
     if (textField == _usernameTF) {
         _usernameTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
         
    }else if (textField == _passwordTF) {
        _passwordTF.layer.borderColor = [[UIColor colorWithRed:218.0f/255.0f green:177.0f/255.0f blue:75.0f/255.0f alpha:1]CGColor];
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    if (textField == _usernameTF) {
        _usernameTF.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    }
    else if (textField == _passwordTF){
        _passwordTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    }
    
    [_usernameTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    
    [_scrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _usernameTF) {
        
        [_passwordTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _passwordTF) {
        
        [_passwordTF resignFirstResponder];
        
        returnValue = YES;
        
    }
    
    return returnValue;
}

//-(void) animateTextField: (UITextField*) textField up: (BOOL) up
//{
//    if (textField!=_usernameTF||textField!=_passwordTF) {
//        const int movementDistance = 105;
//        
//        const float movementDuration = 0.3f;
//        
//        int movement = (up ? -movementDistance : movementDistance);
//        
//        [UIView beginAnimations: @"anim" context: nil];
//        
//        [UIView setAnimationBeginsFromCurrentState: YES];
//        
//        [UIView setAnimationDuration: movementDuration];
//        
//        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
//        
//        [UIView commitAnimations];
//    }
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Forgot Password Servicecall
- (IBAction)forgotPasswordButton:(id)sender {
    
     alertController = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert !"]
                                                                              message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Enter your registered email id"]
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder =[[SharedClass sharedInstance]languageSelectedStringForKey:@"Enter Email Id"];
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeEmailAddress;
    }];
   
    [alertController addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       
        NSArray * textfields = alertController.textFields;
        
        UITextField * namefield = textfields[0];
        
        NSLog(@"jfhgjdjgd %@",namefield);
        
        emailString=namefield.text;
        
      //  // http://voliveafrica.com/noura_services/services/forget_password?email=rajgopal@volivesolutions.com
        
            NSMutableDictionary * forgotPWPostDictionary = [[NSMutableDictionary alloc]init];
        
            [forgotPWPostDictionary setObject:emailString forKey:@"email"];
        
            //[[SharedClass sharedInstance]showprogressfor:@"Loading.."];
        
            [[SharedClass sharedInstance]fetchResponseforParameter:@"forget_password?" withPostDict:forgotPWPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
                NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
                NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
               // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
                NSLog(@"%@",dataFromJson);
                NSLog(@"%@",dataDictionary);
                [SVProgressHUD dismiss];
                if ([status isEqualToString:@"1"])
                {
        
        
                    dispatch_async(dispatch_get_main_queue(), ^{
        
                        NSLog(@"successfully sended to emailid ");
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                                    
                                                                                       message:[[SharedClass sharedInstance]languageSelectedStringForKey:[dataDictionary objectForKey:@"msg"]] preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                           style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                                             
                                                                         }];
                        [alert addAction:okButton];
                        [self presentViewController:alert animated:YES completion:nil];

        
                    });
        
        
                } else {
        
                    dispatch_async(dispatch_get_main_queue(), ^{
        
                        //[SVProgressHUD dismiss];
//                        [[SharedClass sharedInstance]showAlertWithTitle:@"Alert!" withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                        
                        
 UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"]  message:[[SharedClass sharedInstance]languageSelectedStringForKey:[dataDictionary objectForKey:@"msg"]] preferredStyle:UIAlertControllerStyleAlert];
                        
    UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                           style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                                             
                                                                         }];
                        [alert addAction:okButton];
                        [self presentViewController:alert animated:YES completion:nil];
          
                        
                    });
                    
                }
                
            }];
            
        
    }]];
   
    [self presentViewController:alertController animated:YES completion:nil];


}

- (IBAction)rememberMeButton:(id)sender {
    isChecked=!isChecked;
    if (isChecked == YES) {
        _rememberMeCheckBox.image = [UIImage imageNamed:@"check2"];
        
    }else if (isChecked == NO)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"saveEmail"];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"savePassword"];
        _rememberMeCheckBox.image = [UIImage imageNamed:@"checkbox"];
    }
}

-(void)alert :(NSString *)Msg{
    // UIAlertController *controller = [];
    
    alertController = [UIAlertController alertControllerWithTitle:@"" message:Msg preferredStyle:UIAlertControllerStyleAlert];
    
    //UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    //[alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark Login Servicecall
-(void)loginServiceCall
{
    //  http://voliveafrica.com/noura_services/services/login?email=raj@gmail.com&password=123456
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    NSMutableDictionary * loginPostDictionary = [[NSMutableDictionary alloc]init];
    
    [loginPostDictionary setObject:self.usernameTF.text forKey:@"email"];
    [loginPostDictionary setObject:self.passwordTF.text forKey:@"password"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"login?" withPostDict:loginPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        [SVProgressHUD dismiss];
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"store_id"]forKey:@"saveStore"];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"cust_id"]forKey:@"customerId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"email"]forKey:@"emailId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"cart_id"]forKey:@"cartId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"mobile"]forKey:@"mobile"];
                
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(isChecked == YES)
                {
                    
                    [[NSUserDefaults standardUserDefaults]setObject:self.usernameTF.text forKey:@"saveEmail"];
                    [[NSUserDefaults standardUserDefaults]setObject:self.passwordTF.text forKey:@"savePassword"];
                    
                }
                else{
                    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"saveEmail"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"savePassword"];
                    
                }
                
                SWRevealViewController*reveal=[self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                
                [self presentViewController:reveal animated:YES completion:^{    }];
                
                
            });
            
            
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[SVProgressHUD dismiss];
                [objForSharedClass.hud hideAnimated:YES];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                
            });
            
        }
        
    }];

}

- (IBAction)logInBtn:(id)sender {
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if ([emailTest evaluateWithObject:_usernameTF.text] == NO ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter valid Email-Id"] onViewController:self completion:^{  [_usernameTF becomeFirstResponder]; }];
        
    } else if (_passwordTF.text.length < 5 ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Password should be more than 5 characters"] onViewController:self completion:^{  [_passwordTF becomeFirstResponder]; }];
        
    }
    else{
        
        if ([[SharedClass sharedInstance]isNetworkAvalible] == YES) {
            
            [self loginServiceCall];
        }
        else{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"]  message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please check your internet connection"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                style:UIAlertActionStyleDefault
                                    handler:nil];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
            
    }

    }
}

- (IBAction)signUpButton:(id)sender {
    
    RegisterViewController *signUp = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:signUp animated:YES];
}

- (IBAction)changeLanguageBtn:(id)sender {
    
    [self changeLanguage];

}

-(void)changeLanguage
{
    
    alertController = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Change Language"] message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *english = [UIAlertAction actionWithTitle:@"English" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        languageString = [NSString stringWithFormat:@"1"];
       

        [[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
         [self loadViewWithCustomDesign];
        
    }];
    UIAlertAction *arabic = [UIAlertAction actionWithTitle:@"العربية" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        languageString = [NSString stringWithFormat:@"2"];
       
        [[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
         [self loadViewWithCustomDesign];

        
    }];
    
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:english];
    [alertController addAction:arabic];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
}

#pragma mark Guest Login Servicecall
- (IBAction)guestLogin_BTN:(id)sender {

    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //  http://voliveafrica.com/noura_services/services/guest_cart_create?store_id=1
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    NSMutableDictionary * guestPostDictionary = [[NSMutableDictionary alloc]init];
    [guestPostDictionary setObject:languageStr forKey:@"store_id"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"guest_cart_create?" withPostDict:guestPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        [SVProgressHUD dismiss];
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[NSUserDefaults standardUserDefaults]setObject:[dataDictionary objectForKey:@"guest_cart_id"] forKey:@"guestCartId"];
                
                
            });
                SWRevealViewController*reveal=[self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                
                [self presentViewController:reveal animated:YES completion:^{    }];
                
            
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[SVProgressHUD dismiss];
                [objForSharedClass.hud hideAnimated:YES];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
            
        }
        
    }];
   
    
}
@end
