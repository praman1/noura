//
//  ClothsListViewController.m
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ClothsListViewController.h"
#import "ProductsViewController.h"
#import "SharedClass.h"
#import "HomeViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "MBProgressHUD.h"
@interface ClothsListViewController ()
{
    NSMutableArray *clothsListCount;
    NSMutableArray *clothsIdArray;
    SharedClass *objForSharedClass;
}

@end

@implementation ClothsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
   // [self subCategoriesServiceCall];
    
    
    // Do any additional setup after loading the view.
}
-(void)backBtnClciked{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {
    
    objForSharedClass = [[SharedClass alloc] init];
    _clothsListTableview.delegate = self;
    _clothsListTableview.dataSource = self;
    
    clothsListCount = [NSMutableArray new];
    _clothsList = [NSMutableArray new];
    clothsIdArray = [NSMutableArray new];
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnClciked)];
    back.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = back;
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 257, 43)];
    view.backgroundColor = [UIColor clearColor];
    
    UIImageView *imagev = [[UIImageView alloc]initWithFrame:CGRectMake(5, 2, 62, 34)];
    imagev.image = [UIImage imageNamed:@"home logo"];
    [view addSubview:imagev];
    self.navigationItem.titleView = view;
}

#pragma mark SubCategories Servicecall
-(void)subCategoriesServiceCall:(NSString*)string
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    //[objForSharedClass alertforLoading:self];
    
   // http://voliveafrica.com/noura_services/services/sub_categories?cat_id=24
    NSMutableDictionary *subCategoriesPostDictionary = [[NSMutableDictionary alloc]init];
    [subCategoriesPostDictionary setObject:string forKey:@"cat_id"];

    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"sub_categories?" withPostDict:subCategoriesPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"] && [message isEqualToString:@"success"])
        {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });

            dispatch_async(dispatch_get_main_queue(), ^{
                
                clothsListCount=[dataDictionary objectForKey:@"data"];
                
                if (clothsListCount.count>0) {
                    _clothsList = [NSMutableArray new];
                    clothsIdArray = [NSMutableArray new];
                    for (int i=0; i<clothsListCount.count; i++) {
                        [_clothsList addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [clothsIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:_clothsList forKey:@"clothList"];
                        [[NSUserDefaults standardUserDefaults] setObject:clothsIdArray forKey:@"clothsIdList"];
                        
                        NSLog(@"%@",_clothsList);
                        NSLog(@"%@",clothsIdArray);
                    }
                }

                    [_clothsListTableview reloadData];
                
                    });
        }
        else
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                    [SVProgressHUD dismiss];
                    //[objForSharedClass alertforMessage:self :@"cancel@1x" :@"No Data Available!" :@"Ok"];
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"]
                                            
                                                                               message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"No Data Available"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                     
                                                                 }];
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
                
            });
        }
       
    }];

}

#pragma mark Tableview Delegate Methods
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _clothsList.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    //static NSString *identifier = @"clothsListCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"clothsListCell" forIndexPath:indexPath];
    cell.textLabel.text = [_clothsList objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:15];
        return cell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _clothsListTableview)
    {
        ProductsViewController *productsList = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductsViewController"];
        [productsList serviceCall:[clothsIdArray objectAtIndex:indexPath.row]];
        productsList.sub_cat_idArr = clothsIdArray;
      //  productsList.clothNameArr = self.clothsList;
        productsList.clothIDsArr = clothsIdArray;
        
        [self.navigationController pushViewController:productsList animated:YES];
        //[self performSegueWithIdentifier:@"productsPush" sender:self];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
