//
//  PaymentController.m
//  Noura App
//
//  Created by Mohammad Apsar on 8/30/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "PaymentController.h"
#import "CartController.h"
#import "SharedClass.h"

@interface PaymentController ()
{
    NSString * payment_method_str;
    NSString * shipping_method_str;
    NSString *addressString;
    NSString * cust_Id;
    NSString * paymentString;
    NSObject*objectUserDefaults;
    NSString * onlinePaymentString;
    NSString *paymentTypeString;
    NSString *cityStr;
    NSString *countryId;
    NSString *postCodeStr;
    BOOL isChecked;
   
   
}

@end

@implementation PaymentController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];

    isChecked = NO;
    
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {
    
    _onlinePaymentLabel.text = @"Online Payment";
    
    _continueConfirmBtn.layer.masksToBounds = false;
    _continueConfirmBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _continueConfirmBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _continueConfirmBtn.layer.shadowRadius = 3;
    _continueConfirmBtn.layer.shadowOpacity = 0.5;
    
    [_continueConfirmBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"CONTINUE TO CONFIRAMATION"] forState:UIControlStateNormal];
    
    _totalAmountLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Total Amount"];
    
    _cashOnDeliveryLbl.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"label1Str"];
    _FlatRateLbl.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"label2Str"];
    _amountLbl.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"totalPriceStr"];
    
    payment_method_str=[[NSUserDefaults standardUserDefaults]objectForKey:@"value1Str"];
    shipping_method_str=[[NSUserDefaults standardUserDefaults]objectForKey:@"value2Str"];
    NSLog(@"payment_method_str %@  : shipping_method_str %@",payment_method_str,shipping_method_str);
    // [shipping_method_str stringByReplacingOccurrencesOfString:@"""" withString:@""];
    
    
    UITapGestureRecognizer *cashOnDelivery = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cashOnDeliverytapped)];
    
    [self.cashOnDeliveryView addGestureRecognizer:cashOnDelivery];
    
    UITapGestureRecognizer *flatrate = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(flatRateTapped)];
    
    [self.cashOnDeliveryView addGestureRecognizer:flatrate];
    
     objectUserDefaults=[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"];
    
}

-(void)cashOnDeliverytapped{
    
    paymentString =@"cashOnDelivery";
    
}
-(void)flatRateTapped{
    paymentString =@"flatRate";
}

#pragma mark Order Confirmation Servicecall
-(void)continueConfirmServiceCall
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    // http://voliveafrica.com/noura_services/services/order_confirmation?cart_id=246&shipping_method=flatrate_flatrate&payment_method=cashondelivery&store_id=1
    
    if (objectUserDefaults != nil) {
        
        
        NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
        
        [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"cartId"] forKey:@"cart_id"];
        [cartProductPostDictionary setObject:shipping_method_str forKey:@"shipping_method"];
        
        [cartProductPostDictionary setObject:payment_method_str forKey:@"payment_method"];
        
        [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
        //[cartProductPostDictionary setObject:@"1" forKey:@"store_id"];
        
        NSLog(@"cartProductPostDictionary %@",cartProductPostDictionary);
        
        [[SharedClass sharedInstance]fetchResponseforParameter:@"order_confirmation?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"success"]];
            
            NSLog(@"%@",dataFromJson);
            NSLog(@"%@ My Data Is",dataDictionary);
            
            if ([status isEqualToString:@"1"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    
                });
                NSLog(@"my data is printing *****");
                
                addressString=[NSString stringWithFormat:@"%@ ,%@, %@, %@, %@, %@, %@,%@",
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"firstname"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"lastname"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"street"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"city"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"region"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"postcode"],[[dataDictionary objectForKey:@"address"]objectForKey:@"country_id"],[[dataDictionary objectForKey:@"address"]objectForKey:@"telephone"]];
                
                cityStr = [[dataDictionary objectForKey:@"address"]objectForKey:@"city"];
                [[NSUserDefaults standardUserDefaults]setObject:cityStr forKey:@"city"];
                
                countryId = [[dataDictionary objectForKey:@"address"]objectForKey:@"country_id"];
                [[NSUserDefaults standardUserDefaults]setObject:countryId forKey:@"countryId"];
                
                postCodeStr = [[dataDictionary objectForKey:@"address"]objectForKey:@"postcode"];
                [[NSUserDefaults standardUserDefaults]setObject:postCodeStr forKey:@"postCode"];
                

                NSLog(@"myString is %@",addressString);
                
                // [[NSUserDefaults standardUserDefaults] setObject:addressString forKey:@"addressString"];
                
                // cust_Id=[[dataDictionary objectForKey:@"address"]objectForKey:@"customer_id"];
                
                
                // [[NSUserDefaults standardUserDefaults] setObject:cust_Id forKey:@"cust_Id"];
                
                
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                });
            }
            
        }];
    }
    else{
        
        // http://voliveafrica.com/noura_services/services/order_confirmation?cart_id=246&shipping_method=flatrate_flatrate&payment_method=cashondelivery&store_id=1
        NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
        
        [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"guestCartId"] forKey:@"cart_id"];
        
        [cartProductPostDictionary setObject:shipping_method_str forKey:@"shipping_method"];
        
        [cartProductPostDictionary setObject:payment_method_str forKey:@"payment_method"];
        
        [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
        //[cartProductPostDictionary setObject:@"1" forKey:@"store_id"];
        
        NSLog(@"cartProductPostDictionary %@",cartProductPostDictionary);
        
        [[SharedClass sharedInstance]fetchResponseforParameter:@"order_confirmation?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"success"]];
            
            NSLog(@"%@",dataFromJson);
            NSLog(@"%@ My Data Is",dataDictionary);
            
            if ([status isEqualToString:@"1"])
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    
                });
                NSLog(@"my data is printing *****");
                
                addressString=[NSString stringWithFormat:@"%@ ,%@, %@, %@, %@, %@, %@,%@",
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"firstname"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"lastname"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"street"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"city"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"region"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"postcode"],[[dataDictionary objectForKey:@"address"]objectForKey:@"country_id"],[[dataDictionary objectForKey:@"address"]objectForKey:@"telephone"]];
                
                cityStr = [[dataDictionary objectForKey:@"address"]objectForKey:@"city"];
                
                [[NSUserDefaults standardUserDefaults]setObject:cityStr forKey:@"city"];
                
                
                
                NSLog(@"myString is %@",addressString);
                
                // [[NSUserDefaults standardUserDefaults] setObject:addressString forKey:@"addressString"];
                
                // cust_Id=[[dataDictionary objectForKey:@"address"]objectForKey:@"customer_id"];
                
                
                // [[NSUserDefaults standardUserDefaults] setObject:cust_Id forKey:@"cust_Id"];
                
                
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                });
            }
            
        }];
        
    }
    
}



- (IBAction)continueConfirmBtn:(id)sender {
    
    [self continueConfirmServiceCall];
}

#pragma mark COD Button
- (IBAction)COD_BTN:(id)sender {
    
    isChecked=!isChecked;
   // if(isChecked == YES){
        paymentTypeString = @"cod";
    [[NSUserDefaults standardUserDefaults]setObject:paymentTypeString forKey:@"paymentType"];
        _CODRadioBtn.image = [UIImage imageNamed:@"select radio"];
        _onlinePaymentRadioBtn.image = [UIImage imageNamed:@"unselect radio"];
   // }
    
}

#pragma mark Online Payment Button
- (IBAction)onlinePay_BTN:(id)sender {
    
    isChecked=!isChecked;
    //if(isChecked == YES){
        paymentTypeString = @"online";
    [[NSUserDefaults standardUserDefaults]setObject:paymentTypeString forKey:@"paymentType"];
        _onlinePaymentRadioBtn.image = [UIImage imageNamed:@"select radio"];
        _CODRadioBtn.image = [UIImage imageNamed:@"unselect radio"];
    //}
   // onlinePaymentString = @"yes";
    
    
}


@end
