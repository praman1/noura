//
//  MostViewedProductsViewController.h
//  Noura App
//
//  Created by volive solutions on 28/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MostViewedCell.h"

@interface MostViewedProductsViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *mostViewedCollectionView;

- (IBAction)backBtn:(id)sender;



@end
