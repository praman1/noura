//
//  SlideMenuViewController.h
//  Noura App
//
//  Created by volive solutions on 23/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SlideMenuViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)homeButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *slideMenuTableview;
@property NSMutableArray *menuImagesArray, *menuNamesArray;
@property (weak, nonatomic) IBOutlet UILabel *homeLabel;

@end
