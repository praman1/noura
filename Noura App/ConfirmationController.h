//
//  ConfirmationController.h
//  Noura App
//
//  Created by Mohammad Apsar on 8/31/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConfirmationController : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)placeOrderBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *placeBtn;

@property (weak, nonatomic) IBOutlet UILabel *addressLbl;

@property (weak, nonatomic) IBOutlet UITableView *orderDetailsTableView;
@property (weak, nonatomic) IBOutlet UILabel *subTotalLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingLabel;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingChargeLabel;
@property (weak, nonatomic) IBOutlet UILabel *grandTotalAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *placeOrderBtn;
@property (weak, nonatomic) IBOutlet UILabel *shippingToLabel;
@property (weak, nonatomic) IBOutlet UILabel *yourOrderLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentSummeryLabel;
@property (weak, nonatomic) IBOutlet UIView *maskView;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;

@end
