//
//  SimilarProductsCollectionViewCell.h
//  Noura App
//
//  Created by volive solutions on 9/2/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
@interface SimilarProductsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *similarProductImageView;
@property (weak, nonatomic) IBOutlet UILabel *similarProductNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldAmountLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *similarRatingView;

@end
