//
//  MainCartCell.h
//  Noura App
//
//  Created by Mohammad Apsar on 17/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANStepperView.h"

@interface MainCartCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cartProductImage;
@property (weak, nonatomic) IBOutlet UILabel *cartProductName;
@property (weak, nonatomic) IBOutlet UILabel *cartProductPrice;
@property (weak, nonatomic) IBOutlet ANStepperView *stepperValueLbl;

@property (weak, nonatomic) IBOutlet UIButton *removeCartProductBtn;


@end
