//
//  SlideMenuViewController.m
//  Noura App
//
//  Created by volive solutions on 23/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "SlideMenuViewController.h"
#import "SlideMenuTableViewCell.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "LoginViewController.h"
#import "TabBarController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "SettingsController.h"
#import "SharedClass.h"
@interface SlideMenuViewController ()
{
    SlideMenuTableViewCell *menuItemCell;
    AppDelegate * menuAppDelegate;
    NSString *type;
    
}

@end

@implementation SlideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    menuAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    [self loadViewWithCustomDesign];
    // Do any additional setup after loading the view.
}

#pragma mark LoadViewWithCustomDesign
-(void)loadViewWithCustomDesign {
    
    _slideMenuTableview.delegate = self;
    _slideMenuTableview.dataSource = self;
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
         _menuNamesArray = [[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"My Account"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"Change Password"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"About Us"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"Logout"], nil];
        
    } else  {
        
        _menuNamesArray = [[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"My Account"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"Change Password"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"About Us"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"Logout"], nil];

    }
    _homeLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Home"];
    
    _menuImagesArray = [[NSMutableArray alloc]initWithObjects:@"menu my account",@"menu settings",@"menu about",@"menu logout", nil];
   
    
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    if ([segue.identifier isEqualToString:@"pushToSettings"]) {
//        SettingsController *obj = [[SettingsController alloc] init];
//        obj = [segue destinationViewController];
//        obj.type = type;
//    }
//   
//    
//}

//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 0;
//}

#pragma mark Tableview Delegate Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_menuNamesArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    menuItemCell = [tableView dequeueReusableCellWithIdentifier:@"menuItemCell"];
    if(menuItemCell == nil)
    {
    menuItemCell = [[SlideMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menuItemCell"];
    }
    menuItemCell.menuImageView.image =[UIImage imageNamed:[_menuImagesArray objectAtIndex:indexPath.row]];
    menuItemCell.menuNameLabel.text = [_menuNamesArray objectAtIndex:indexPath.row];
    
    return menuItemCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     menuItemCell = [tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:{
            menuAppDelegate.selectTabItem = @"0";
            [self performSegueWithIdentifier:@"pushToProfile" sender:self];
            break;
         }
        case 1:{
            type = @"settings";
            
            [[NSUserDefaults standardUserDefaults]setValue:@"settings" forKey:@"about"];
            [self performSegueWithIdentifier:@"pushToSettings" sender:self];
            break;
        }
        case 2:{
            type = @"about";
            [[NSUserDefaults standardUserDefaults]setValue:@"about" forKey:@"about"];
            [self performSegueWithIdentifier:@"pushToSettings" sender:self];
           // [self performSegueWithIdentifier:@"pushToAboutUs" sender:self];
            break;
        }
        case 3:{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Logout"]
                                         message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Are you sure to Logout?"]
                                         preferredStyle:UIAlertControllerStyleActionSheet];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"YES"]
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                    
                                            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"customerId"];
                                            
                                            
                                            UIViewController * gotoMainNav = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavVC"];
                                            [[UIApplication sharedApplication] delegate].window.rootViewController = gotoMainNav;
                                            [[[UIApplication sharedApplication] delegate].window makeKeyAndVisible];
                                            
                                           // [self clearAllData];
                                        }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle no, thanks button
                                       }];
            
            //Add your buttons to alert controller
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            
            [self presentViewController:alert animated:YES completion:nil];
            break;
        }
        default:
            break;
}

}

- (IBAction)homeButton:(id)sender {
    menuAppDelegate.selectTabItem = @"0";
    HomeViewController * home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:home animated:YES];
    
}
@end
