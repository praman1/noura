//
//  CartController.h
//  Noura App
//
//  Created by Mohammad Apsar on 8/30/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface CartController : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *shippingViewBtn;
@property (weak, nonatomic) IBOutlet UIView *confirmationViewBtn;
@property (weak, nonatomic) IBOutlet UIView *paymentViewBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *shippingView;
@property (weak, nonatomic) IBOutlet UIView *shippingBtmView;
@property (weak, nonatomic) IBOutlet UIView *confirmationBtmView;
@property (weak, nonatomic) IBOutlet UIView *paymentBtmView;

@property (weak, nonatomic) IBOutlet UILabel *lblShipping;
@property (weak, nonatomic) IBOutlet UILabel *lblPayment;
@property (weak, nonatomic) IBOutlet UILabel *lblConfirmation;
@property (weak, nonatomic) IBOutlet UIImageView *imgShipping;
@property (weak, nonatomic) IBOutlet UIImageView *imgPayment;
@property (weak, nonatomic) IBOutlet UIImageView *imgConfirmation;

- (IBAction)continuePayBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *firstName;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTF;
@property (weak, nonatomic) IBOutlet UITextField *companyTF;

@property (weak, nonatomic) IBOutlet UITextField *phoneTF;
@property (weak, nonatomic) IBOutlet UITextField *addressTF;
@property (weak, nonatomic) IBOutlet UITextField *cityTF;
@property (weak, nonatomic) IBOutlet UITextField *pincodeTF;

@property (weak, nonatomic) IBOutlet UITextField *stateTF;
@property (weak, nonatomic) IBOutlet UITextField *countryTF;

@property (weak, nonatomic) IBOutlet UIView *AddressView;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentController;
- (IBAction)segmentAction:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *savedAddressTV;
@property (weak, nonatomic) IBOutlet UIButton *continuePayBtn;
@property (weak, nonatomic) IBOutlet UILabel *shippingLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentLabel;
@property (weak, nonatomic) IBOutlet UILabel *confirmationLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingAddressLabel;



@end
