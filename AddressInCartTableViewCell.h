//
//  AddressInCartTableViewCell.h
//  Noura App
//
//  Created by Volive solutions on 10/10/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressInCartTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *AddressLabel;

@end
