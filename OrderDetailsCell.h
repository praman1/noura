//
//  OrderDetailsCell.h
//  Noura App
//
//  Created by volive solutions on 21/10/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
